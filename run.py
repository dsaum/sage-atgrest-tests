# coding: utf-8

import unittest
import xmlrunner
import string
import random
import requests
import sys
import env_config
import json
import logging
import util
import datetime

API_ROOT = env_config.API_ROOT
HEADERS = { 'Content-Type': 'application/json' }
AUTH = requests.auth.HTTPBasicAuth(env_config.AUTH[0], env_config.AUTH[1])

rconf = { 'headers': HEADERS, 'auth': AUTH, 'verify': False }

class AbstractTestCase(unittest.TestCase):
    '''Base class for verification tests'''

    def compare(self, o1, o2, props):
        for k in props:
            if o1.has_key(k):
                self.assertEqual(o1[k], o2[k])

    def create_user(self, data, account_type=None):
        logging.debug('creating user: ' + str(data['profile']))
        url = API_ROOT + '/profile'
        if account_type != None:
            url = url + '?accountType=' + account_type
        logging.debug('at url: ' + url)
        response = requests.post(url, data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.text)
        logging.debug('user created: %s' % response_data['profile']['id'])
        logging.debug(response.text)
        return response_data

    def create_department(self):
        department = {
            'department': {
                'address': {
                    'address1': 'Cyncoed Road',
                    'billingAddress': True,
                    'city': 'Cardiff',
                    'country': 'GB',
                    'county': 'South Glamorgan',
                    'firstName': 'Russell',
                    'institution': 'University of Wales Institute, Cardiff',
                    'lastName': 'Deacon',
                    'phoneNumber': '01222551111',
                    'postalCode': 'CF23 6XD',
                    'salutation': 'Dr',
                    'shippingAddress': True,
                    'departmentType': '10123'
                },
                'departmentTypeId' : '0369748218000000',
                'institutionId': '149000',
            }
        }
        url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        response = requests.post(url, data = json.dumps(department), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['department']

    def create_course(self, data = {}):
        self.department = self.create_department()
        course_status = 'PA' + str(datetime.datetime.now().year + 1)[2:]
        course = {
            'course' : {
                'name': 'Introduction to Statistics',
                'departmentId': self.department['id'],
                'courseLevel': '13000001',
                'category1': '600',
                'category2': '610',
                'category3': '617',
                'expectedEnrollment' : 300,
                'courseNumber': 'Econ 101',
                'status' : course_status
            }
        }
        course['course'].update(data)
        url = API_ROOT + '/profile/%s/courses' % self.profile_id
        response = requests.post(url, data = json.dumps(course), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['course']

class ProfileTests(AbstractTestCase):
    '''tests to **rconfy
        - create
        - update
        - login
        - resetPassword'''

    def test_create_user_extended_success(self):
        user = util.random_user()
        user['profile'].update({
            'middleName': 'Lynn',
            'firstName': 'Sarah',
            'lastName': 'Warnes',
            'telephone': '0123456789',
            'title': 'Professor',
            'organisationName': 'Bob Jones University',
            'areasOfInterest' : ['A00','AA0','A70'],
            'roles' : ['18'],
            'locale': 'en_US',
            'country' : 'US',
            'address': {
                'salutation': 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'institution': 'California Lutheran University',
                'roomNumber': '12',
                'building': 'Beinecke Library',
                'department': 'Computer Science Dept',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'address2': 'Room 10',
                'address3': 'PO Box 45',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : True,
                'state': 'CA',
                'country':'US'
            }
        })
        logging.debug(user)
        response = requests.post(API_ROOT + '/profile', data = json.dumps(user), **rconf)
        if response.status_code not in [200,201]:
            logging.warn(response.text)
        else:
            logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        print(response.headers)
        data = json.loads(response.text)
        u1 = user['profile']
        u2 = data['profile']

        props = ['email', 'salutation', 'firstName', 'lastName', 'middleName',
                    'telephone', 'organisationName', 'areasOfInterest',
                    'roles',  'locale', 'country']

        self.compare(u1, u2, props)

    def test_create_user_missing_country(self):
        self.email = util.get_username('test_') + '@sagepub.com'
        self.password = util.random_string()
        user = {
            'profile': {
                'email' : self.email,
                'password': self.password,
                'firstName': 'Jill',
                'lastName': 'Doe',
                'telephone': '0123456789',
            }
        }
        response = requests.post(API_ROOT + '/profile', data = json.dumps(user), **rconf)
        self.assertEqual(400, response.status_code)
        data = json.loads(response.text)
        self.assertEqual('missing_required_field', data['error']['key'])
        self.assertEqual('country', data['error']['field'])

    def test_create_corwin_user(self):
        user = self.create_user(util.random_user(), account_type="corwin")
        self.assertEqual(user.get('profile').get('userType'), 'corwinUser')

    def test_update_user(self):
        user = self.create_user(util.random_user())
        new_fields = {
            'firstName' : 'John',
            'lastName'  : 'Doe',
            'telephone' : '+230948234',
            'salutation': 'Dr',
            'middleName': 'Jeremiah',
            'title': 'Professor Emeritus',
            'organisationName': 'Bob Jones University',
            'areasOfInterest' : ['CB0','CA0','C70'],
            'roles' : ['18',],
            'locale': 'en_US',
            'country' : 'CA',
        }
        for f in new_fields.keys():
            user['profile'][f] = new_fields.get(f)
        url = API_ROOT + '/profile/' + user['profile'].get('id')
        print json.dumps(user)
        response = requests.put(url, data = json.dumps(user), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        self.compare(user, json.loads(response.text), new_fields.keys())

    def test_update_user_change_email(self):
        """confirms that
           a) we can change the email
           b) the user can still sign in with the new email address"""
        user = util.random_user()
        password = user['profile']['password']
        user = self.create_user(user)
        new_email = util.get_username('test_') + '@sagepub.com'
        user['profile']['email'] = new_email
        url = API_ROOT + '/profile/' + user['profile'].get('id')
        response = requests.put(url, data = json.dumps(user), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = {'login' : {'email' : new_email, 'password': password}}
        logging.debug(data)
        response = requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

    def test_update_password_success(self):
        """confirms that
           a) we can change the password
           b) the user can still sign in with the new password"""
        user = util.random_user()
        email = user['profile']['email']
        password = user['profile']['password']
        user = self.create_user(user)
        new_password = util.random_string()
        pw_data = { 'passwordUpdate' : {
            'password' : password,
            'newPassword' : new_password
        } }
        url = API_ROOT + '/profile/' + user['profile'].get('id') + '/updatePassword'
        response = requests.post(url, data = json.dumps(pw_data), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = {'login' : {'email' : email, 'password': new_password}}
        logging.debug(data)
        response = requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

    def test_update_password_invalid_credentials(self):
        user = util.random_user()
        email = user['profile']['email']
        password = user['profile']['password']
        user = self.create_user(user)
        new_password = util.random_string()
        pw_data = { 'passwordUpdate' : {
            'password' : 'an_invalid_password',
            'newPassword' : new_password
        } }
        url = API_ROOT + '/profile/' + user['profile'].get('id') + '/updatePassword'
        response = requests.post(url, data = json.dumps(pw_data), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.text)
        self.assertEqual(data['error']['key'], 'invalid_credentials')

    def test_create_user_user_exists(self):
        user = util.random_user()
        email = user['profile']['email']
        self.create_user(user)

        response = requests.post(API_ROOT + '/profile', data = json.dumps(user), **rconf)
        self.assertEqual(response.status_code, 409)
        data = json.loads(response.text)
        self.assertEqual('username_already_in_use', data['error']['key'])
        self.assertEqual(email, data['error']['email'])

    def test_login_success(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        password = user['profile']['password']
        data = {'login' : {'email' : email, 'password': password}}
        response = requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 200)

    def test_login_ensure_last_activity_updated(self):
        user = util.random_user()
        email = user['profile']['email']
        password = user['profile']['password']
        user = self.create_user( user )
        profile_id = user['profile']['id']
        orig_last_active = user['profile']['lastActivity']
        data = {'login' : {'email' : email, 'password': password}}
        requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        # retrieve the user and make the sure the field was updated
        url = API_ROOT + '/profile/' + profile_id
        response = requests.get(url, **rconf)
        response_data = json.loads(response.text)
        logging.debug(response.text)
        last_active = response_data['profile']['lastActivity']
        self.assertTrue(last_active is not None)
        self.assertTrue(last_active != orig_last_active)

    def test_login_corwin_success(self):
        user = util.random_user()
        self.create_user( user, account_type='corwin' )
        email = user['profile']['email']
        password = user['profile']['password']
        data = {'login' : {'email' : email, 'password': password}}
        response = requests.post(API_ROOT + '/profile/login?accountTypes=corwin', data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 200)

    def test_login_corwin_existing_account(self):
        password = '123456'
        email = 'darcysaum@gmail.com'
        data = {'login' : {'email' : email, 'password': password }}
        response = requests.post(API_ROOT + '/profile/login?accountTypes=corwin', data = json.dumps(data), **rconf)
        print response.text
        self.assertEqual(response.status_code, 200)

    def test_login_fail(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        data = {'login' : {'email' : email, 'password': 'some_invalid_password'}}
        response = requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.text)
        self.assertEqual('invalid_credentials', data['error']['key'])

    def test_get_user(self):
        self.email = util.get_username('test_') + '@sagepub.com'
        self.password = util.random_string()
        user = {
            'profile': {
                'email' : self.email,
                'password': self.password,
                'firstName': 'Jill',
                'lastName': 'Doe',
                'telephone': '0123456789',
                'address1': '35 Wright Lane',
                'city': 'Thousand Oaks',
                'postCode': '93210',
                'state': 'CA',
                'country': 'US',
                'locale': 'en_US'
            }
        }
        first_response = requests.post(API_ROOT + '/profile', data = json.dumps(user), **rconf)

        data = {'login' : {'email' : self.email, 'password': 'some_invalid_password'}}
        response = requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 400)
        data = json.loads(response.text)
        self.assertEqual('invalid_credentials', data['error']['key'])

    def test_reset_password(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        url = (API_ROOT + '/profile/resetPassword/%s' % email)
        response = requests.post(url, **rconf)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        self.assertEqual(data['response']['reset_password_request'], 'sent')

    def test_create_password_reset_token(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        url = (API_ROOT + '/profile/passwordReset/token/%s' % email)
        response = requests.post(url, **rconf)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        self.assertTrue(data['response'].get('token') is not None)

    def test_validate_password_reset_token(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        url = (API_ROOT + '/profile/passwordReset/token/%s' % email)
        response = requests.post(url, **rconf)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        token = data['response'].get('token')
        self.assertTrue(data['response'].get('token') is not None)


    def test_reset_password_using_token(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        url = (API_ROOT + '/profile/passwordReset/token/%s' % email)
        response = requests.post(url, **rconf)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        token = data['response'].get('token')
        new_pass = util.random_string(size=8)
        update = { 'passwordUpdate' : {
            'password' : new_pass,
            'confirm' : new_pass
        } }
        url = (API_ROOT + '/profile/passwordReset/token/%s' % token)
        response = requests.put(url, data=json.dumps(update), **rconf)
        print response.text
        data = json.loads(response.text)
        logging.debug(response.text)
        self.assertEquals(data['response']['password_changed'], 'true')
        data = {'login' : {'email' : email, 'password': new_pass}}
        response = requests.post(API_ROOT + '/profile/login', data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 200)

    def test_reset_password_using_token_expired_token(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        url = (API_ROOT + '/profile/passwordReset/token/%s?expires=0' % email)
        response = requests.post(url, **rconf)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        token = data['response'].get('token')
        new_pass = util.random_string(size=8)
        update = { 'passwordUpdate' : {
            'password' : new_pass,
            'confirm' : new_pass
        } }
        # just make it so that the token expires now
        url = (API_ROOT + '/profile/passwordReset/token/%s' % token)
        response = requests.put(url, data=json.dumps(update), **rconf)
        data = json.loads(response.text)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 403)
        self.assertEquals(data['error']['key'], 'token_expired')


    def test_reset_password_using_token_token_already_used(self):
        user = util.random_user()
        self.create_user( user )
        email = user['profile']['email']
        url = (API_ROOT + '/profile/passwordReset/token/%s' % email)
        response = requests.post(url, **rconf)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        token = data['response'].get('token')
        new_pass = util.random_string(size=8)
        update = { 'passwordUpdate' : {
            'password' : new_pass,
            'confirm' : new_pass
        } }
        url = (API_ROOT + '/profile/passwordReset/token/%s' % token)
        response = requests.put(url, data=json.dumps(update), **rconf)
        # discard the first response then call the password reset again
        # with the same token
        response = requests.put(url, data=json.dumps(update), **rconf)
        data = json.loads(response.text)
        self.assertEquals(response.status_code, 403)
        self.assertEquals(data['error']['key'], 'token_invalid')

    def test_reset_password_using_token_bad_token(self):
        new_pass = util.random_string(size=8)
        update = { 'passwordUpdate' : {
            'password' : new_pass,
            'confirm' : new_pass
        } }
        url = (API_ROOT + '/profile/passwordReset/token/%s' % 'ATOKENIMADEUPMYSELF')
        response = requests.put(url, data=json.dumps(update), **rconf)
        data = json.loads(response.text)
        self.assertEquals(response.status_code, 403)
        self.assertEquals(data['error']['key'], 'token_notfound')

    def test_create_corwin_profile(self):
        grade_levels = ['1', '2', '4']
        business_type = 'PreK-12 (School Based)'
        user = util.random_user()
        user['profile'].update({
            'middleName': 'Lynn',
            'firstName': 'Sarah',
            'lastName': 'Warnes',
            'telephone': '0123456789',
            'title': 'Professor',
            'organisationName': 'Bob Jones University',
            'areasOfInterest' : ['A00','AA0','A70'],
            'gradeLevels' : grade_levels,
            'businessType' : business_type,
            'roles' : ['18'],
            'locale': 'en_US',
            'country' : 'US',
            'address': {
                'salutation': 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'institution': 'California Lutheran University',
                'roomNumber': '12',
                'building': 'Beinecke Library',
                'department': 'Computer Science Dept',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'address2': 'Room 10',
                'address3': 'PO Box 45',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : True,
                'state': 'CA',
                'country':'US'
            }
        })
        logging.debug(user)
        response = requests.post(API_ROOT + '/profile?accountType=corwin', data = json.dumps(user), **rconf)
        if response.status_code not in [200,201]:
            logging.warn(response.text)
        else:
            logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        new_user = data['profile']
        self.assertEqual(new_user.get('gradeLevels').sort(), grade_levels.sort())
        self.assertEqual(new_user.get('businessType'), business_type)

class DepartmentTests(AbstractTestCase):
    '''tests for manipulating departments'''
    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']

    def create_department(self, dept=None):
        if dept == None:
            dept = {
                'department': {
                    'address': {
                        'address1': 'Cyncoed Road',
                        'billingAddress': True,
                        'city': 'Cardiff',
                        'country': 'GB',
                        'county': 'South Glamorgan',
                        'building' : 'One Earls Court',
                        'firstName': 'Russell',
                        'institution': 'University of Wales Institute, Cardiff',
                        'lastName': 'Deacon',
                        'phoneNumber': '01222551111',
                        'postalCode': 'CF23 6XD',
                        'salutation': 'Dr',
                        'shippingAddress': True,
                        'departmentType': '10123',
                        'telephone': '123094820934820938'
                    },
                    'departmentTypeId' : '0369748218000000',
                    'institutionId': '149000',
                }
            }
        url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        response = requests.post(url, data = json.dumps(dept), **rconf)
        logging.debug(response.text)
        if response.status_code not in [200,201]:
            logging.warn(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['department']

    def department(self):
        department = {
            'department': {
                'address': {
                    'address1': 'Cyncoed Road',
                    'billingAddress': True,
                    'city': 'Cardiff',
                    'country': 'GB',
                    'county': 'South Glamorgan',
                    'building' : 'One Earls Court',
                    'firstName': 'Russell',
                    'institution': 'University of Wales Institute, Cardiff',
                    'lastName': 'Deacon',
                    'phoneNumber': '01222551111',
                    'postalCode': 'CF23 6XD',
                    'salutation': 'Dr',
                    'shippingAddress': True
                },
                'departmentTypeId' : '0369748218000000',
                'institutionId': '03697482',
            }
        }
        data = self.create_department(department)
        dept = department['department']
        self.compare(dept, data, ['departmentId','institutionId','departmentTypeId'])
        self.assertEqual(data['name'], 'Computer Programs')
        self.assertEqual(data['institutionName'], 'Santa Barbara Business College')

        addr = dept['address']
        addr_data = data['address']
        self.compare(addr, addr_data,\
                    ['addressType', 'address1', 'billingAddress', 'city', 'country'\
                     'building', 'firstName', 'lastName', 'phoneNumber', 'postalCode',\
                     'salutation', 'shippingAddress', ])
        self.assertEqual(addr_data['department'], 'Computer Programs')
        self.assertEqual(addr_data['institution'], 'Santa Barbara Business College')

        # confirm that it was added to the profile
        list_url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        list_res = requests.get(list_url, **rconf)
        logging.info(list_res.text)
        list_data = json.loads(list_res.text)
        self.assertEqual(len(list_data['departments']), 1)


    def test_create_department_no_address(self):
        department = {
            'department': {
                'address': {
                    'country': 'GB',
                },
                'departmentTypeId' : '0369748218000000',
                'institutionId': '03697482',
            }
        }
        data = self.create_department(department)
        dept = department['department']
        self.compare(dept, data, ['departmentId','institutionId','departmentTypeId'])
        self.assertEqual(data['name'], 'Computer Programs')
        self.assertEqual(data['institutionName'], 'Santa Barbara Business College')

        # confirm that it was added to the profile
        list_url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        list_res = requests.get(list_url, **rconf)
        logging.debug(list_res.text)
        list_data = json.loads(list_res.text)
        self.assertEqual(len(list_data['departments']), 1)


    def test_update_department_relations(self):
        """update a department by changing the dept type or institution id"""
        department = {
            'department': {
                'address': {
                    'address1': 'Cyncoed Road',
                    'billingAddress': True,
                    'city': 'Cardiff',
                    'country': 'GB',
                    'county': 'South Glamorgan',
                    'building' : 'One Earls Court',
                    'firstName': 'Russell',
                    'lastName': 'Deacon',
                    'phoneNumber': '01222551111',
                    'postalCode': 'CF23 6XD',
                    'salutation': 'Dr',
                    'shippingAddress': True
                },
                'departmentTypeId' : '0369748218000000',
                'institutionId': '03697482',
            }
        }
        data = self.create_department(department)
        new_fields = {
            'departmentTypeId' : '0157099346500000',
            'institutionId': '01570993',
        }
        for f in new_fields.keys():
            data[f] = new_fields.get(f)
        url = API_ROOT + ('/profile/%s/departments/%s' % (self.profile_id, data['id']))

        update_response = requests.put(url, data=json.dumps({'department': data}), **rconf)
        logging.debug(update_response.text)
        self.assertEqual(update_response.status_code,200)
        update_data = json.loads(update_response.text)
        self.compare(update_data['department'], new_fields, ['departmentTypeId','institutionId'])
        # from sps_department_type
        self.assertEqual(update_data['department']['name'], 'History Dept')
        self.assertEqual(update_data['department']['institutionName'], 'Grace College')

    def test_update_department_fields(self):
        """update a dept address by directly manipulating the address fields"""
        department = {
            'department': {
                'address': {
                    'address1': 'Cyncoed Road',
                    'billingAddress': True,
                    'city': 'Cardiff',
                    'country': 'GB',
                    'county': 'South Glamorgan',
                    'building' : 'One Earls Court',
                    'firstName': 'Russell',
                    'institution': 'University of Wales Institute, Cardiff',
                    'lastName': 'Deacon',
                    'phoneNumber': '01222551111',
                    'postalCode': 'CF23 6XD',
                    'salutation': 'Dr',
                    'shippingAddress': True
                },
                'departmentTypeId' : '0369748218000000',
                'institutionId': '03697482',
            }
        }
        data = self.create_department(department)
        new_fields = {
            'address1': 'Cardiff Road',
            'billingAddress': False,
            'city': 'Glasgow',
            'country': 'GB',
            'county': 'North Glamorgan',
            'building' : 'Highbury Stadium',
            'firstName': 'Terry',
            'institution': 'Some place other',
            'lastName': 'Smith',
            'phoneNumber': '203948230948',
            'postalCode': '10012',

            'salutation': 'Mrs',
            'shippingAddress': False
        }
        for f in new_fields.keys():
            data['address'][f] = new_fields.get(f)
        url = API_ROOT + ('/profile/%s/departments/%s' % (self.profile_id, data['id']))

        update_response = requests.put(url, data=json.dumps({'department': data}), **rconf)
        logging.debug(update_response.text)
        self.assertEqual(update_response.status_code,200)
        update_data = json.loads(update_response.text)
        self.compare(update_data['department']['address'], new_fields, new_fields.keys())


    def test_create_multiple_departments(self):
        self.create_department()
        self.create_department()
        self.create_department()

        list_url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        list_res = requests.get(list_url, **rconf)
        logging.debug(list_res.text)
        list_data = json.loads(list_res.text)
        self.assertEqual(len(list_data['departments']), 3)

    def test_delete_departments(self):
        self.create_department()
        self.create_department()
        self.create_department()

        list_url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        list_res = requests.get(list_url, **rconf)
        logging.debug(list_res.text)
        list_data = json.loads(list_res.text)

        depts = list_data['departments']
        delete_id = depts[ depts.keys()[0] ]['id']
        len_before_del = len(depts)
        logging.debug('deleting department ' + delete_id)
        delete_url = API_ROOT + ('/profile/%s/departments/%s' % (self.profile_id, delete_id))
        delete_res = requests.delete(delete_url, **rconf)
        self.assertEqual(delete_res.status_code, 200)
        delete_data = json.loads(delete_res.text)
        self.assertEqual(delete_data['response']['deleted'], True)

        list_url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        list_res = requests.get(list_url, **rconf)
        logging.debug(list_res.text)
        list_data = json.loads(list_res.text)

        self.assertEqual(len(list_data['departments']), len_before_del - 1)

    def test_delete_departments_then_create_again(self):
        self.create_department()

        list_url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        list_res = requests.get(list_url, **rconf)
        logging.debug(list_res.text)
        list_data = json.loads(list_res.text)

        depts = list_data['departments']
        delete_id = depts[ depts.keys()[0] ]['id']
        len_before_del = len(depts)
        logging.debug('deleting department ' + delete_id)
        delete_url = API_ROOT + ('/profile/%s/departments/%s' % (self.profile_id, delete_id))
        delete_res = requests.delete(delete_url, **rconf)
        self.assertEqual(delete_res.status_code, 200)
        delete_data = json.loads(delete_res.text)
        self.assertEqual(delete_data['response']['deleted'], True)

        # make sure we can create another one
        self.create_department()

    def test_create_department_for_user_institution(self):
        department = {
            'department': {
                'name' : 'Department of social work',
                "institutionId" : "355700001",
                'address': {
                    'address1': 'Cyncoed Road',
                    'billingAddress': True,
                    'city': 'Cardiff',
                    'country': 'GB',
                    'county': 'South Glamorgan',
                    'firstName': 'Russell',
                    'institution': 'University of Wales Institute, Cardiff',
                    'lastName': 'Deacon',
                    'phoneNumber': '01222551111',
                    'postalCode': 'CF23 6XD',
                    'salutation': 'Dr',
                    'shippingAddress': True,
                    'departmentType': '10123'
                }
            }
        }
        url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        logging.debug(json.dumps(department))
        response = requests.post(url, data = json.dumps(department), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['department']


class SchoolTests(AbstractTestCase):
    '''tests for manipulating Corwin K-12 schools and districts'''
    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']

    def test_create_school(self, dept=None):
        if dept == None:
            dept = {
                'school': {
                    'address': {
                        'billingAddress': True,
                        'country' : 'US',
                        'firstName': 'Russell',
                        'lastName': 'Deacon',
                        'phoneNumber': '01222551111',
                        'shippingAddress': True,
                        'school': 'TREE OF LIFE MONT CHRTR SCHOOL',
                        'district': 'New Haven School District'
                    },
                    'schoolId' : '04919661',
                    'name' : 'TREE OF LIFE MONT CHRTR SCHOOL'
                }
            }
        url = API_ROOT + ('/profile/%s/schools' % self.profile_id)
        response = requests.post(url, data = json.dumps(dept), **rconf)
        logging.debug(response.text)
        if response.status_code not in [200,201]:
            logging.warn(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['school']

    def test_create_district(self, dept=None):
        if dept == None:
            dept = {
                'school': {
                    'address': {
                        'address1': '5530 E NORTHERN LIGHTS BLVD',
                        'billingAddress': True,
                        'city': 'Anchorage',
                        'country': 'US',
                        'state' : 'AK',
                        'firstName': 'John',
                        'lastName': 'Doe',
                        'phoneNumber': '907-742-4000',
                        'postalCode': '99504-3135',
                        'salutation': 'Dr',
                        'shippingAddress': True,
                        'telephone': '907-742-4000'
                    },
                    'districtId' : '00000024',
                    'name' : 'ANCHORAGE SCHOOL DISTRICT'
                }
            }
        url = API_ROOT + ('/profile/%s/schools' % self.profile_id)
        response = requests.post(url, data = json.dumps(dept), **rconf)
        logging.debug(response.text)
        if response.status_code not in [200,201]:
            logging.warn(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['school']

class AddressTests(AbstractTestCase):
    '''tests for manipulating addresses'''

    def setUp(self):
        new_user = util.random_user(extended=False)
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']

    def test_create_address(self):

        address = {
            'address' : {
                'salutation': 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'institution': 'California Lutheran University',
                'roomNumber': '12',
                'department': 'Computer Science Dept,',
                'companyName': 'SAGE Publications',
                'pobox' : True,
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'address2': 'Room 10',
                'address3': 'PO Box 45',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : True,
                'billingAddress' : True,
                'state': 'CA',
                'country':'US'
            }
        }

        url = (API_ROOT + '/profile/%s/addresses' % self.profile_id)
        response = requests.post(url, data = json.dumps(address), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

        user_addresses = requests.get(url, **rconf)
        logging.debug(user_addresses.text)
        data = json.loads(user_addresses.text)
        self.assertEqual(1, len(data['addresses']))



    def test_update_address(self):
        original = {
            'address' : {
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : True,
                'billingAddress' : False,
                'state': 'CA',
                'country':'US'
            }
        }
        url = (API_ROOT + '/profile/%s/addresses' % self.profile_id)
        response = requests.post(url, data = json.dumps(original), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)

        new_fields = {
            'firstName': 'Jennifer',
            'lastName': 'Thomas',
            'telephone': '+230497832',
            'address1': '10 Fifth Ave.',
            'city': 'New York',
            'postCode': '06517',
            'shippingAddress' : False,
            'billingAddress' : True,
            'state': 'NY',
            'country':'US',
            'companyName':'SAGE Publications',
            'pobox': True,
            'homeAddress' : True
        }
        for f in new_fields.keys():
            data['address'][f] = new_fields.get(f)
        url = (API_ROOT + '/profile/%s/addresses/%s' % (self.profile_id, data['address']['id']))
        update_response = requests.put(url, data = json.dumps(data), **rconf)
        logging.debug(update_response.text)
        self.assertEqual(update_response.status_code, 200)
        update_data = json.loads(update_response.text)
        self.compare(update_data['address'], new_fields, new_fields.keys())


    def test_create_addresses(self):
        address_1 = {
            'address' : {
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : True,
                'billingAddress' : False,
                'state': 'CA',
                'country':'US'
            }
        }
        address_2 = {
            'address' : {
                'firstName': 'John Doe',
                'lastName': 'Warnes',
                'department': 'Computer Science Dept,',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : False,
                'billingAddress' : True,
                'state': 'CA',
                'country':'US',
                'homeAddress' : True
            }
        }
        url = (API_ROOT + '/profile/%s/addresses' % self.profile_id)
        r_1 = requests.post(url, data = json.dumps(address_1), **rconf)
        logging.debug(r_1.text)
        r_2 = requests.post(url, data = json.dumps(address_2), **rconf)
        logging.debug(r_1.text)
        user_addresses = requests.get(url, **rconf)
        self.addresses = json.loads(user_addresses.text)['addresses']
        logging.debug(user_addresses.text)
        self.assertEqual(2, len(self.addresses))

    def test_update_home_address(self):
        """confirms that if one address is created and set as the home
           address, then another one is added as the home address, the
           first will no longer have homeAddress==true"""
        address_1 = {
            'address' : {
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : True,
                'billingAddress' : False,
                'state': 'CA',
                'country':'US',
                'homeAddress': True
            }
        }
        url = (API_ROOT + '/profile/%s/addresses' % self.profile_id)
        r_1 = requests.post(url, data = json.dumps(address_1), **rconf)
        data_1 = json.loads(r_1.text)
        addr_id = data_1['address']['id']
        logging.debug(r_1.text)
        self.assertTrue(data_1['address']['homeAddress'])
        address_2 = {
            'address' : {
                'firstName': 'John Doe',
                'lastName': 'Warnes',
                'department': 'Computer Science Dept,',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'shippingAddress' : False,
                'billingAddress' : True,
                'state': 'CA',
                'country':'US',
                'homeAddress' : True
            }
        }
        r_2 = requests.post(url, data = json.dumps(address_2), **rconf)
        logging.debug(r_2.text)
        self.assertTrue(json.loads(r_2.text)['address']['homeAddress'])
        url = url + '/' + addr_id
        response = requests.get(url, **rconf)
        new_data = json.loads(response.text)['address']
        self.assertFalse(new_data['homeAddress'])


    def test_delete_address(self):
        self.test_create_addresses()
        addr_id = self.addresses[1]['id']
        logging.debug('deleting address ' + addr_id)
        url = (API_ROOT + '/profile/%s/addresses/%s' % (self.profile_id, addr_id))
        r = requests.delete(url, **rconf)
        self.assertEqual(r.status_code, 200)
        deletion = json.loads(r.text)
        self.assertEqual(deletion['response']['deleted'], True)

        list_url = API_ROOT + '/profile/%s/addresses' % self.profile_id
        response = requests.get(list_url, **rconf)
        logging.debug(response.text)
        data = json.loads(response.text)
        self.assertEqual(1, len(data['addresses']))

class CourseTests(AbstractTestCase):
    """tests out course modification stuff"""

    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']

    def test_create_course(self):
        dept = self.create_department()
        course = {
            'course' : {
                'name': 'Introduction to Statistics',
                'departmentId': dept['id'],
                'courseLevel': '13000001',
                'category1': '600',
                'category2': '610',
                'category3': '617',
                'expectedEnrollment' : 300,
                'courseNumber': 'Econ 101',
                'status' : 'PS16'
            }
        }
        url = API_ROOT + '/profile/%s/courses' % self.profile_id
        response = requests.post(url, data = json.dumps(course), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

        list_url = API_ROOT + '/profile/%s/courses' % self.profile_id
        response = requests.get(list_url, **rconf)
        logging.debug(response.text)
        data = json.loads(response.text)
        self.assertEqual(1, len(data['courses']))

    def test_update_course(self):
        dept_1 = self.create_department()
        course = {
            'course' : {
                'name': 'Introduction to Statistics',
                'departmentId': dept_1['id'],
                'courseLevel': '13000001',
                'category1': '600',
                'category2': '610',
                'category3': '617',
                'expectedEnrollment' : 300,
                'courseNumber': 'Econ 101',
                'status' : 'PS16'
            }
        }
        url = API_ROOT + '/profile/%s/courses' % self.profile_id
        response = requests.post(url, data = json.dumps(course), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)

        dept_2 = self.create_department()
        new_fields = {
            'name' : 'Regression Techniques',
            'departmentId' : dept_2['id'],
            'courseLevel' : '19800002',
            'category1': '700',
            'category2': '710',
            'category3': '717',
            'expectedEnrollment' : '10',
            'courseNumber': 'Econ 304',
            'status' : 'PS16'
        }
        for f in new_fields.keys():
         data['course'][f] = new_fields.get(f)
        url = API_ROOT + '/profile/%s/courses/%s' % (self.profile_id, data['course']['id'])
        update_response = requests.put(url, data = json.dumps(data), **rconf)
        logging.debug(update_response.text)
        update_data = json.loads(update_response.text)
        self.assertEqual(update_response.status_code, 200)
        self.compare(new_fields, update_data['course'], new_fields.keys())


    def test_delete_course(self):
        url = API_ROOT + '/profile/%s/courses' % self.profile_id
        dept = self.create_department()
        course_1 = {
            'course' : {
                'name': 'Introduction to Statistics',
                'departmentId': dept['id'],
                'courseLevel': '13000001',
                'category1': '600',
                'category2': '610',
                'category3': '617',
                'expectedEnrollment' : 300,
                'courseNumber': 'Econ 101',
                'status' : 'PS16'
            }
        }
        requests.post(url, data = json.dumps(course_1), **rconf)
        course_2 = {
            'course' : {
                'name': 'Advanced Statistics',
                'departmentId': dept['id'],
                'courseLevel': '13000001',
                'category1': '600',
                'category2': '610',
                'category3': '617',
                'expectedEnrollment' : 300,
                'courseNumber': 'Econ 301',
                'status' : 'PS16'
            }
        }
        response = requests.post(url, data = json.dumps(course_2), **rconf)
        logging.debug(response.text)
        data = json.loads(response.text)
        course_id = data['course']['id']
        logging.debug('deleting course ' + course_id)
        delete_url = API_ROOT + \
                ('/profile/%s/courses/%s' % (self.profile_id, course_id))
        del_response = requests.delete(delete_url, **rconf)
        self.assertEqual(del_response.status_code, 200)
        logging.debug(del_response.text)
        del_data = json.loads(del_response.text)
        self.assertEqual(del_data['response']['deleted'], True)
        list_url = API_ROOT + '/profile/%s/courses' % self.profile_id
        response = requests.get(list_url, **rconf)
        data = json.loads(response.text)
        self.assertEqual(1, len(data['courses']))


class ICFRTests(AbstractTestCase):
    """Tests for ICFR orders"""
    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']
        self.course = self.create_course()


    def test_create_order(self):
        """verifies the creation of a single order and double checks
           that all the fields are copied correctly"""
        order = {
            "icfrorder": {
                "commerceItems": [{
                  "ISBN-13": "9781446249185",
                  "catalogKey": "en_GB",
                  "catalogRefId": "Book Product49790",
                  "courseInstances": [{
                    "courseId": self.course['id'],
                    "courseStatus": "A",
                    "courseNum": "Hist101",
                    "decisionDate": "2014-11-05",
                    "departmentId": self.department['id'],
                    "endDate": "2014-02-01",
                    "enrollment": "23",
                    "startDate": "2014-01-01"
                  }],
                  "icfrType": "E",
                  "requestType": "Normal",
                  "adoptionType": "Regular Opportunity",
                  "productId": "Book238032",
                  "quantity": 1
                }],
                "shippingAddressId": self.department['address']['id']
            }
        }

        logging.debug(json)
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)['icfrorder']
        self.compare( data['shippingGroups'].values()[0],\
                      self.department['address'],\
                      ['address1','address2','address3','lastName',
                       'firstName','country','county','city'])
        self.assertEqual(data['state'], 'SUBMITTED')
        self.assertTrue( len(data['commerceItems']) == 1 )
        commerceItem = data['commerceItems'].values()[0]
        self.compare(commerceItem, order['icfrorder']['commerceItems'][0],\
                ['productId','catalogRefId','quantity','icfrType','ISBN-13',
                 'catalogKey'])
        self.assertTrue(len(commerceItem.get('courseInstances')) == 1)
        courseInstance = commerceItem.get('courseInstances')[0]



    def test_create_cq_mixedorder(self):
        """verifies the creation of a single order and double checks
           that all the fields are copied correctly"""
        order = {
            "icfrorder": {
                "commerceItems": [
                    {
                        "attributes": {
                            "bindingType": "P"
                        },
                        "catalogKey": "en_US",
                        "catalogRefId": "Book Product43853",
                        "productId": "Book236228",
                        "icfrType": "E",
                        "requestType": "Normal",
                        "adoptionType": "Regular Opportunity",
                        "courseInstances": [{
                          "courseId": self.course['id'],
                          "courseStatus": "A",
                          "courseNum": "Hist101",
                          "decisionDate": "2014-11-05",
                          "departmentId": self.department['id'],
                          "endDate": "2014-02-01",
                          "enrollment": "23",
                          "startDate": "2014-01-01"
                        }],
                        "quantity": "1"
                    }
                ],
                "shippingAddressId": self.department['address']['id']
            }
        }

        logging.debug(json)
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)

    def test_create_multiple_orders_and_list(self):
        o1 = { "icfrorder": {
            "shippingAddressId": self.department['address']['id'],
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "requestType": "Normal",
                "adoptionType": "Regular Opportunity",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_GB",
                "courseInstances": [ {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseNum": "Hist101",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-29"
                } ]
            } ]
        } }
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        r_1 = requests.post(url, data = json.dumps(o1), **rconf)
        self.assertEqual(r_1.status_code, 200)
        r_2 = requests.post(url, data = json.dumps(o1), **rconf)
        self.assertEqual(r_2.status_code, 200)
        r_3 = requests.post(url, data = json.dumps(o1), **rconf)
        self.assertEqual(r_3.status_code, 200)
        # get the list of items
        response = requests.get(url, **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        self.assertEqual(len(data['icfrorders']), 3)

    def test_create_order_multiple_course_instances_same_course(self):
        order = { "icfrorder": {
            "shippingAddressId": self.department['address']['id'],
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "requestType": "Normal",
                "adoptionType": "Regular Opportunity",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_GB",
                "courseInstances": [ {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-29"
                } , {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-29"
                } ]
            } ]
        } }
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        res = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(res.text)
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.text)
        # confirm that only one courseinstance was created for the
        # commerce item
        commerce_item_id = data['icfrorder']['commerceItems'].keys()[0]
        self.assertEqual(len(data['icfrorder']['commerceItems'][commerce_item_id]['courseInstances']),1)

    def test_create_order_multiple_commerce_items(self):
        order = { "icfrorder": {
            "shippingAddressId": self.department['address']['id'],
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "requestType": "Normal",
                "adoptionType": "Regular Opportunity",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_GB",
                "courseInstances": [ {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-29"
                } ]
            }, {
                "productId": "Book243249",
                "catalogRefId": "Book Product76122",
                "quantity": 1,
                "icfrType": "E",
                "ISBN-13": "9781446295731",
                "catalogKey": "en_GB",
                "courseInstances": [ {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-29"
                }  ]
            } ]
        } }
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        res = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(res.text)
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.text)
        # confirm that only one courseinstance was created for the
        # commerce item
        self.assertEqual(len(data['icfrorder']['commerceItems']),2)

    def test_get_order(self):
        order = { "icfrorder": {
            "shippingAddressId": self.department['address']['id'],
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "requestType": "Normal",
                "adoptionType": "Regular Opportunity",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_GB",
                "courseInstances": [ {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-29"
                } ]
            } ]
        } }
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)

        order_url = url + '/' + data['icfrorder']['id']
        response = requests.get(order_url, **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

    def test_get_orders_user_with_no_orders(self):
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        response = requests.get(url, **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

class ECommerceTests(AbstractTestCase):
    """Tests for e-commerce orders from anonymous users"""
    def setUp(self):
        pass

    def test_validation_order(self):
        user = util.random_user()['profile']
        order = { 'order' : {
            'email' : user['email'],
            'locale':'en_US',
            'shippingAddress' : {
                'salutation' : 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'phoneNumber': '0123456789',
                'email' : user['email'],
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postalCode': '90210',
                'state': 'AL',
                'country':'US'
            },
            'billingAddress' : {
                              'salutation' : 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postalCode': '90210',
                'state': 'AL',
                'country':'US',
                'phoneNumber': '0123456789',
                'email' : user['email'],
            },
            "commerceItems": [ {
                "productId": "Book241785",
                "catalogRefId": "Book Product66713",
                "quantity": 1,
                "ISBN-13": "9781483344393",
                "catalogKey": "en_US",
                'amount' : {
                    'listPrice' : 74.0,
                    'taxLines' : [{
                        'type' : 'state',
                        'rate' : 6.0,
                        'amount' : 4.44
                    }],
                    'amount' : 74.0
                }
            },{
                "productId": "Book242507",
                "catalogRefId": "Book Product71974",
                "quantity": 1,
                "ISBN-13": "9781483358666",
                "catalogKey": "en_US",
                'amount' : {
                    'listPrice' : 39.0,
                    'amount' : 39.0,
                    'taxLines' : [{
                        'type' : 'state',
                        'rate' : 6.0,
                        'amount' : 1.87
                    }],
                    'discounts' : [{
                      'code' : 'S05CAES',
                      'type' : 'percent',
                      'amount' : 7.80,
                      'rate' : 0.2
                    }]
                }
            } ],
            'amount' :{
                'amount' : 118.94,
            },
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
            "shipping" : {
              'code' : 'UPSP-GROUND',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
                'taxLines' : [ {
                  'type' : 'country',
                  'rate' : 6.0,
                  'amount' : 0.48
                } ]
              }
            }
        } }
        url = API_ROOT + ('/orders')
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 200)

    def test_validation_order_gb(self):
        user = util.random_user()['profile']
        order = { 'order' : {
            'email' : user['email'],
            'locale':'en_GB',
            'shippingAddress' : {
                              'prefix' : 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'phoneNumber': '0123456789',
                'email' : user['email'],
                'address1': '10 Sachem St.',
                'city': 'Saint-Maur-des-Fosses',
                'postalCode': '94100',
                'state': '',
                'country':'FR'
            },
            'billingAddress' : {
                              'prefix' : 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'address1': '10 Sachem St.',
                'city': 'Saint-Maur-des-Fosses',
                'postalCode': '94100',
                'state': '',
                'country':'FR',
                'phoneNumber': '0123456789',
                'email' : user['email']
            },
            "commerceItems": [ {
                "productId": "Book241785",
                "catalogRefId": "Book Product66713",
                "quantity": 1,
                "ISBN-13": "9781483344393",
                "catalogKey": "en_GB",
                'onSale' : False,
                'amount' : {
                    'listPrice' : 74.0,
                    'taxLines' : [{
                        'type' : 'vat',
                        'rate' : 6.0,
                        'amount' : 4.44
                    }],
                    'amount' : 74.0
                }
            },{
                "productId": "Book242507",
                "catalogRefId": "Book Product71974",
                "quantity": 1,
                "ISBN-13": "9781483358666",
                "catalogKey": "en_GB",
                'amount' : {
                    'listPrice' : 39.0,
                    'amount' : 39.0,
                    'onSale' : False,
                    'taxLines' : [{
                        'type' : 'vat',
                        'rate' : 6.0,
                        'amount' : 1.87
                    }],
                    'discounts' : [{
                      'code' : 'S05CAES',
                      'type' : 'percent',
                      'amount' : 7.80,
                      'rate' : 0.2
                    }]
                }
            } ],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
            "shipping" : {
              'code' : 'EUR - Airmail Shipping',
              'title' : 'Airmail Shipping',
              'amount' : {
                'amount' : 7.00,
              }
            },
            'amount' : {
                'amount' : 118.51
            }
        } }
        url = API_ROOT + ('/orders')
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 200)

    def test_end_to_end_order(self):
        order = {
          "order" : {
            "locale" : "en_US",
            "email" : "satheesh.chintala@sagepub.co.uk",
            "shippingAddress" : {
                              'saluation' : 'Mrs.',
              "firstName" : "Candler",
              "lastName" : "Huddleston",
              "phoneNumber": "0123456789",
              "address1" : "917 Homewood Drive Unit 5",
              "city" : "Tuscaloosa",
              "state" : "AL",
              "postalCode" : "35401",
              "country" : "US",
              "email" : "satheesh.chintala@sagepub.co.uk"
            },
            "billingAddress" : {
                              'salutation' : 'Mrs.',
              "firstName" : "Carlos",
              "lastName" : "Arango",
              "phoneNumber": "0123456789",
              "address1" : "1724 David Street",
              "city" : "Pensacola",
              "state" : "AL",
              "postalCode" : "32514",
              "country" : "US",
              "email" : "satheesh.chintala@sagepub.co.uk"
            },
            "shipping" : {
              'code' : 'USPS-GROUND',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            },
            "commerceItems" : [ {
              "productId" : "Book242528",
              "catalogRefId" : "Book Product72468",
              "quantity" : 1,
              "ISBN-13": "9781483345642",
              "catalogKey" : "en_US",
              "amount" : {
                "listPrice" : 25.0,
                "salePrice" : 0.0,
                "onSale" : False,
                "amount" : 25.00
              }
            } ],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
            'amount' : {
                'amount' : 100.0
            }
          }
        }
        url = url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data = json.dumps(order), **rconf)
        taxed_order = json.loads(response.text)['order']
        total_amount = util.total_for_amount(taxed_order['shipping'])
        for item in taxed_order['commerceItems']:
            total_amount += util.total_for_amount(item)
        taxed_order['amount'] = {'amount' : total_amount}
        url = API_ROOT + ('/orders')
        response = requests.post(url, data = json.dumps({'order': taxed_order}), **rconf)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 200)

    def test_create_order(self):
        user = util.random_user()['profile']
        order = { 'order' : {
            'email' : user['email'],
            'locale':'en_US',
            'shippingAddress' : {
                              'prefix' : 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            'billingAddress' : {
                              'prefix' : 'Mrs.',
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            'amount' :{
                'amount' : 55.24
            },
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_US",
                'amount' : {
                    'listPrice' : 50.0,
                    'salePrice' : 45.0,
                    'onSale' : True,
                    'taxLines' : [{
                        'type' : 'state',
                        'rate' : 2.0,
                        'amount' : 0.9
                    },{
                        'type' : 'city',
                        'rate' : 5.0,
                        'amount' : 2.25
                    }],
                    'amount' : 45.00
                }
            } ],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
                'tax' : 0.14,
                'total' : 7.09,
                'taxLines' : [ {
                  'type' : 'country',
                  'rate' : 2.0,
                  'amount' : 0.14
                } ]
              }
            }
        } }
        url = API_ROOT + ('/orders')
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 200)

    def test_create_mixed_journal_book_order(self):
      user = util.random_user()['profile']
      order = { 'order' : {
          'email' : user['email'],
          'locale':'en_US',
          'shippingAddress' : {
                            'prefix' : 'Mrs.',
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'phoneNumber': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'Los Angeles',
              'postCode': '90210',
              'state': 'CA',
              'country':'US'
          },
          'billingAddress' : {
                            'prefix' : 'Mrs.',
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'phoneNumber': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'Los Angeles',
              'postCode': '90210',
              'state': 'CA',
              'country':'US',
              'institution' : 'University of Oregon'
          },
          'amount' :{
              'amount' : 820.03,
          },
          "commerceItems": [ {
              "productId": "Book239229",
              "catalogRefId": "Book Product56587",
              "quantity": 1,
              "catalogKey": "en_US",
              'amount' : {
                  'listPrice' : 108.0,
                  'taxLines' : [{
                      'type' : 'state',
                      'rate' : 6.0,
                      'amount' : 6.48
                  }],
                  'amount' : 108.0
              }
          } , {
              "productId": "Journal201763",
              "catalogRefId": "14Journal201763P11",
              "quantity": 1,
              "catalogKey": "en_US",
              'amount' : {
                  'listPrice' : 660.0,
                  'taxLines' : [{
                      'type' : 'state',
                      'rate' : 6.0,
                      'amount' : 39.6
                  }],
                  'amount' : 660.0
              }
          }],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
          "shipping" : {
            'code' : 'UPS- GRND RESIDENTIAL',
            'title' : 'UPS- GRND RESIDENTIAL',
            'amount' : {
              'amount' : 5.95,
              'total' : 5.95,
              'tax' : 0.0
            }
          }
      } }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)

    def test_create_mixed_journal_book_order_uk(self):
      user = util.random_user()['profile']
      logging.debug('creating order for user: %s' % (user['email']))
      order = { 'order' : {
          'locale' : 'en_GB',
          'email' : user['email'],
          'shippingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'London',
              'postCode': 'NE1G7A',
              'country':'GB'
          },
          'billingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'London',
              'postCode': 'NE1G7A',
              'country':'GB'
          },
          'amount' :{
              'amount' : 233.99,
          },
          "commerceItems": [ {
              "productId": "Book239287",
              "catalogRefId": "Book Product56587",
              "quantity": 1,
              "catalogKey": "en_GB",
              'amount' : {
                  'listPrice' : 34.99,
                  'amount' : 34.99
              }
          } , {
              "productId": "Journal200959",
              "catalogRefId": "50Journal200959P11",
              "quantity": 1,
              "catalogKey": "en_GB",
              'amount' : {
                  'listPrice' : 199.0,
                  'amount' : 199.0
              }
          }],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
          "shipping" : {
            'code' : 'Free Shipping',
            'title' : 'Free Shipping',
            'amount' : {
              'amount' : 0,
              'total' : 0,
              'tax' : 0
            }
          }
      } }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)

    def test_create_discount_book_order_with_quantity_greater_than_1(self):
      user = util.random_user()['profile']
      logging.debug('creating order for user: %s' % (user['email']))
      order = { 'order' : {
          'locale' : 'en_GB',
          'email' : user['email'],
          'shippingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'London',
              'postCode': 'NE1G7A',
              'country':'GB'
          },
          'billingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'London',
              'postCode': 'NE1G7A',
              'country':'GB'
          },
          'amount' :{
              'amount' : 83.98,
          },
          "commerceItems": [ {
              "productId": "Book239287",
              "catalogRefId": "Book Product56587",
              "quantity": 3,
              "catalogKey": "en_GB",
              'amount' : {
                  'discounts' : [{
                    'code' : 'S05CAES',
                    'type' : 'percent',
                    'amount' : 20.994,
                    'rate' : 0.2
                  }],
                  'listPrice' : 34.99,
                  'taxLines' : [],
                  'amount' : 104.97
              }
          } ],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
          "shipping" : {
            'code' : 'Free Shipping',
            'title' : 'Free Shipping',
            'amount' : {
              'amount' : 0,
              'total' : 0,
              'tax' : 0
            }
          }
      } }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)


    def test_create_order_no_verify_amount(self):
      user = util.random_user()['profile']
      logging.debug('creating order for user: %s' % (user['email']))
      order = { 'order' : {
          'locale' : 'en_GB',
          'email' : user['email'],
          'shippingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'London',
              'postCode': 'NE1G7A',
              'country':'GB'
          },
          'billingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'London',
              'postCode': 'NE1G7A',
              'country':'GB'
          },
          'amount' :{
              'amount' : 1000000.91,
              'verify' : False
          },
          "commerceItems": [ {
              "productId": "Book239287",
              "catalogRefId": "Book Product56587",
              "quantity": 3,
              "catalogKey": "en_GB",
              'amount' : {
                  'listPrice' : 34.99,
                  'amount' : 104.97
              }
          } ],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
          "shipping" : {
            'code' : 'Free Shipping',
            'title' : 'Free Shipping',
            'amount' : {
              'amount' : 0,
              'total' : 0,
              'tax' : 0
            }
          }
      } }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)


    def test_create_order_canada_ship_tax(self):
        user = util.random_user()['profile']
        order = { "order":{
            "locale":"en_US",
            "commerceItems":[{
                "productId":"Book241833",
                "catalogRefId":"Book Product66903",
                "quantity":"1",
                "catalogKey":"en_US",
                "amount":{
                    "listPrice":50,
                    "amount":50,
                    "taxLines":[{
                        "type":"HST",
                        "rate":5,
                        "amount":2.5
                    }]
                },
                "attributes":{"bindingType":"P"}
            }],
            "shipping":{
                "code":"UPS- CANADA GRND",
                "title":"UPS Standard",
                "amount":{
                    "listPrice":11.95,
                    "amount":11.95,
                    "taxLines":[ {
                        "type":"HST",
                        "rate":13,
                        "amount":1.5535
                    }]
                }
            },
            "payment":{
                "paymentToken":{
                    "type":"paymentToken",
                    "authorizationId":"tst518",
                    "token":"02c86f886e623542ad1eb60224dda819643e1e7e",
                    "source":"paymentech",
                    "txRef":"5550BDF505F36520B512EBAAED32BBA799675304",
                    "attributes":{
                        "cc_holder": "S CHINTALA",
                        "cc_brand":"Visa",
                        "cc_bin":"",
                        "cc_last4":"XXXXXXXXXXXX1116",
                        "cc_expiry_month":"04",
                        "cc_expiry_year":"2016"
                    }
                }
            },
            "email":"satheesh.chintala@sagepub.co.uk",
            "shippingAddress":{
                "firstName":"SATHEESH",
                "lastName":"CHINTALA",
                "telephone":"41303935671",
                "address1":"588 Booth Strret",
                "city":"Ottawa",
                "postCode":"K1A084",
                "state":"ON",
                "country":"CA"
            },
            "billingAddress":{
                "firstName":"SATHEESH",
                "lastName":"CHINTALA",
                "telephone":"41303935671",
                "address1":"588 Booth Strret",
                "city":"Ottawa",
                "postCode":"K1A084",
                "state":"ON",
                "country":"CA"
            },
            "amount": {
                "amount" : 66
            }
        } }
        url = API_ROOT + ('/orders')
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 200)


    def test_create_mixed_cq_book_order(self):
      user = util.random_user()['profile']
      logging.debug('creating order for user: %s' % (user['email']))
      order = {  "order": {
          "email": user['email'],
          "amount": {
              "amount": 131.48
          },
          "billingAddress": {
              'building' : 'Beinecke',
              'roomNumber': '2507',
              "address1": "2455 Teller Road",
              "city": "Thousand Oaks",
              "country": "US",
              "firstName": "Gavin",
              "lastName": "FowlerTest0201",
              "phoneNumber": "",
              "postalCode": "91320",
              "salutation": "Mr",
              "state": "CA"
          },
          "commerceItems": [
              {
                  "amount": {
                      "amount": 43.95,
                      "listPrice": 43.95,
                      "taxLines": [
                          {
                              "amount": 0.44,
                              "rate": 1,
                              "type": "county"
                          },
                          {
                              "amount": 2.86,
                              "rate": 6.5,
                              "type": "state"
                          }
                      ]
                  },
                  "attributes": {
                      "bindingType": "P"
                  },
                  "catalogKey": "en_US",
                  "catalogRefId": "Book Product43853",
                  "productId": "Book236228",
                  "quantity": "1"
              },
              {
                  "amount": {
                      "amount": 35,
                      "listPrice": 35,
                      "taxLines": [
                          {
                              "amount": 0.35,
                              "rate": 1,
                              "type": "county"
                          },
                          {
                              "amount": 2.28,
                              "rate": 6.5,
                              "type": "state"
                          }
                      ]
                  },
                  "attributes": {
                      "bindingType": "P"
                  },
                  "catalogKey": "en_US",
                  "catalogRefId": "Book Product43855",
                  "productId": "Book236230",
                  "quantity": "1"
              },
              {
                  "amount": {
                      "amount": 35.95,
                      "listPrice": 35.95,
                      "taxLines": [
                          {
                              "amount": 0.36,
                              "rate": 1,
                              "type": "county"
                          },
                          {
                              "amount": 2.34,
                              "rate": 6.5,
                              "type": "state"
                          }
                      ]
                  },
                  "attributes": {
                      "bindingType": "P"
                  },
                  "catalogKey": "en_US",
                  "catalogRefId": "Book Product78518",
                  "productId": "Book244099",
                  "quantity": "1"
              }
          ],
          "locale": "en_US",
          "payment": {
              "paymentToken": {
                  "attributes": {
                      "cc_bin": "",
                      "cc_brand": "Visa",
                      "cc_expiry_month": "01",
                      "cc_expiry_year": "2016",
                      "cc_holder": "Gavin Fowler Test 0201",
                      "cc_last4": "XXXXXXXXXXXX4113",
                      "orbitalCustomerRefNum": "58528179",
                      "origin": "drupal",
                      "origin_order_id": "111772",
                      "origin_profile_id": "guest4f8cf4b6edc4cfa"
                  },
                  "authorizationId": "tst141",
                  "source": "paymentech",
                  "token": "da1ba7c2bfb06f071756eab67ff1a8fc6f149b6d",
                  "txRef": "5660641CA83B4B1EE0D1B389043943AB7DF4535E",
                  "type": "paymentToken"
              }
          },
          "shipping": {
              "amount": {
                  "amount": 7.95,
                  "listPrice": 7.95
              },
              "code": "UPS- GRND RESIDENTIAL",
              "title": "UPS Ground"
          },
          "shippingAddress": {
              'building' : 'Beinecke',
              'roomNumber': '2507',
              "address1": "2455 Teller Road",
              "city": "Thousand Oaks",
              "country": "US",
              "firstName": "Gavin",
              "lastName": "FowlerTest0201",
              "phoneNumber": "",
              "postalCode": "91320",
              "salutation": "Mr",
              "state": "CA"
          }
        }
      }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)

    def test_order_ship_discount_cq_mixed(self):
      user = util.random_user()['profile']
      order = { 'order' : {
          'locale':'en_US',
          'email' : user['email'],
          "amount": {
              "amount": 123.53
          },
          "billingAddress": {
              "address1": "2455 Teller Road",
              "city": "Thousand Oaks",
              "country": "US",
              "firstName": "Gavin",
              "lastName": "FowlerTest0201",
              "phoneNumber": "",
              "postalCode": "91320",
              "salutation": "Mr",
              "state": "CA"
          },
          "shippingAddress": {
              "address1": "2455 Teller Road",
              "city": "Thousand Oaks",
              "country": "US",
              "firstName": "Gavin",
              "lastName": "FowlerTest0201",
              "phoneNumber": "",
              "postalCode": "91320",
              "salutation": "Mr",
              "state": "CA"
          },
        "commerceItems": [
            {
                "amount": {
                    "amount": 43.95,
                    "listPrice": 43.95,
                    "taxLines": [
                        {
                            "amount": 0.44,
                            "rate": 1,
                            "type": "county"
                        },
                        {
                            "amount": 2.86,
                            "rate": 6.5,
                            "type": "state"
                        }
                    ]
                },
                "attributes": {
                    "bindingType": "P"
                },
                "catalogKey": "en_US",
                "catalogRefId": "Book Product43853",
                "productId": "Book236228",
                "quantity": "1"
            },
            {
                "amount": {
                    "amount": 35,
                    "listPrice": 35,
                    "taxLines": [
                        {
                            "amount": 0.35,
                            "rate": 1,
                            "type": "county"
                        },
                        {
                            "amount": 2.28,
                            "rate": 6.5,
                            "type": "state"
                        }
                    ]
                },
                "attributes": {
                    "bindingType": "P"
                },
                "catalogKey": "en_US",
                "catalogRefId": "Book Product43855",
                "productId": "Book236230",
                "quantity": "1"
            },
            {
                "amount": {
                    "amount": 35.95,
                    "listPrice": 35.95,
                    "taxLines": [
                        {
                            "amount": 0.36,
                            "rate": 1,
                            "type": "county"
                        },
                        {
                            "amount": 2.34,
                            "rate": 6.5,
                            "type": "state"
                        }
                    ]
                },
                "attributes": {
                    "bindingType": "P"
                },
                "catalogKey": "en_US",
                "catalogRefId": "Book Product78518",
                "productId": "Book244099",
                "quantity": "1"
            }
        ],
        'payment' : {
            'paymentToken' : {
                'authorizationId' : 'tst590',
                'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                'source' : 'paymenttech',
                'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                'attributes' : {
                  'cc_holder' : 'Jamie Dimon',
                  'cc_brand' : 'Visa',
                  'cc_bin' : '407704',
                  'cc_last4' : '1112',
                  'cc_expiry_month' : '06',
                  'cc_expiry_year' : '2015'
                }
            }
        },
        "shipping": {
            "amount": {
                "amount": 7.95,
                "listPrice": 7.95,
                "discounts" : [{
                  'code' : 'C151H7',
                  'amount' : 7.95
                }]
            },
            "code": "UPS- GRND RESIDENTIAL",
            "title": "UPS Ground"
        }
        } }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)

    def test_create_cq_only_book_order(self):
      user = util.random_user()['profile']
      logging.debug('creating order for user: %s' % (user['email']))
      order = { 'order' : {
          'locale' : 'en_US',
          'email' : user['email'],
          'shippingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'New Haven',
              'postCode': '06511',
              'state': 'CT',
              'country':'US'
          },
          'billingAddress' : {
              'firstName': 'Sarah',
              'lastName': 'Warnes',
              'telephone': '0123456789',
              'address1': '10 Sachem St.',
              'city': 'New Haven',
              'postCode': '06511',
              'state': 'CT',
              'country':'US'
          },
          'amount' :{
              'amount' : 56.13,
          },
         "commerceItems": [{
             "amount": {
                 "amount": 43.95,
                 "listPrice": 43.95,
                 "onSale": False,
                 "salePrice": 0.0,
                 "taxLines": [
                     {
                         "amount": 2.6370000000000005,
                         "rate": 6.0,
                         "type": "State tax"
                     }
                 ],
                 "verify": True
             },
             "catalogKey": "en_US",
             "catalogRefId": "Book Product43853",
             "productId": "Book236228",
             "quantity": 1
         }
        ],
        'payment' : {
            'paymentToken' : {
                'authorizationId' : 'tst590',
                'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                'source' : 'paymenttech',
                'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                'attributes' : {
                  'cc_holder' : 'Jamie Dimon',
                  'cc_brand' : 'Visa',
                  'cc_bin' : '407704',
                  'cc_last4' : '1112',
                  'cc_expiry_month' : '06',
                  'cc_expiry_year' : '2015'
                }
            }
        },
        "shipping" : {
            'code' : 'USPS- PRIORITY MAIL',
            'title' : 'Priority Mail',
            'amount' : {
              'amount' : 9.0,
              'total' : 9.0,
              'taxLines': [{
                  'type': 'State tax',
                  'rate': 6.0,
                  'amount': 0.54}]
            }
          }
      } }
      url = API_ROOT + ('/orders')
      response = requests.post(url, data = json.dumps(order), **rconf)
      logging.debug(response.text)
      self.assertEquals(response.status_code, 200)



class SignedInECommerceTests(AbstractTestCase):
    """Tests for e-commerce orders for registered users"""
    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']

    def test_create_order(self):
        order = { 'order' : {
            'locale':'en_US',
            'profileId' : self.profile_id,
            'shippingAddress' : {
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            'billingAddress' : {
                'firstName': 'Sarah',
                'lastName': 'Warnes',
                'telephone': '0123456789',
                'address1': '10 Sachem St.',
                'city': 'Los Angeles',
                'postCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            'amount' :{
                'amount' : 55.29,
            },
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_US",
                'amount' : {
                    'listPrice' : 50.0,
                    'taxLines' : [{
                        'type' : 'state',
                        'rate' : 2.0,
                        'amount' : 0.9
                    },{
                        'type' : 'city',
                        'rate' : 5.0,
                        'amount' : 2.3
                    }],
                    'amount' : 45.00
                }
            } ],
            'payment' : {
                'paymentToken' : {
                    'authorizationId' : 'tst590',
                    'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                    'source' : 'paymenttech',
                    'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                    'attributes' : {
                      'cc_holder' : 'Jamie Dimon',
                      'cc_brand' : 'Visa',
                      'cc_bin' : '407704',
                      'cc_last4' : '1112',
                      'cc_expiry_month' : '06',
                      'cc_expiry_year' : '2015'
                    }
                }
            },
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
                'tax' : 0.14,
                'taxLines' : [ {
                  'type' : 'country',
                  'rate' : 2.0,
                  'amount' : 0.14
                } ]
              }
            }
        } }
        url = API_ROOT + '/orders'
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEquals(response.status_code, 200)
        submitted_order = json.loads(response.text)
        order_id = submitted_order['order']['id']
        url = API_ROOT + '/orders/' + order_id
        response = requests.get(url, **rconf)
        # now retrieve the order and make sure its assigned to the correct user
        retrieved_order = json.loads(response.text)
        self.assertEquals(retrieved_order['order']['profileId'], self.profile_id)



class CloseTheLoopTests(AbstractTestCase):
    """Tests for close-the-loop/followup"""
    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']
        self.course = self.create_course()

    def create_order(self):
        order = { "icfrorder": {
            "shippingAddressId": self.department['address']['id'],
            "commerceItems": [ {
                "productId": "Book226139",
                "catalogRefId": "Book Product25808",
                "quantity": 1,
                "icfrType": "E",
                "ISBN-13": "9780761929987",
                "catalogKey": "en_GB",
                "courseInstances": [ {
                    "courseId": self.course['id'],
                    "departmentId": self.department['id'],
                    "enrollment": "100",
                    "readingLevel": "Recommended Reading",
                    "courseStatus": "10",
                    "decisionDate": "2014-08-24",
                    "startDate": "2014-09-16",
                    "endDate": "2014-11-30"
                } ]
            } ]
        } }
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        response = requests.post(url, data = json.dumps(order), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        return data

    def test_add_followup_adopted_existing_course(self):
        order = self.create_order()
        order_id = order['icfrorder']['id']
        commerce_item = order['icfrorder']['commerceItems']
        commerce_item_id = order['icfrorder']['commerceItems'].keys()[0]
        course_instance = order['icfrorder']['commerceItems'][commerce_item_id]['courseInstances'][0]
        adoption_details = { 'icfrFollowup' :{
            'orderId' : order_id,
            'commerceItemId' : commerce_item_id,
            'adoptionDetails' : {
                'status' : 'adopted',
                'courseAdoptionStatus' : 'Essential',
                'adoptedAs' : 'Essential',
                'reason' : 'I was drinking.',
                'courseInstance' : course_instance,
                'courseInstanceId' : course_instance['id'],
            }
        } }
        url = API_ROOT + ('/profile/%s/icfr/followup' % self.profile_id)
        response = requests.post(url, data=json.dumps(adoption_details), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)


    def test_add_followup_adopted_modify_existing_course(self):
        order = self.create_order()
        order_id = order['icfrorder']['id']
        commerce_item = order['icfrorder']['commerceItems']
        commerce_item_id = order['icfrorder']['commerceItems'].keys()[0]
        course_instance = order['icfrorder']['commerceItems'][commerce_item_id]['courseInstances'][0]
        start_ci_id =         course_instance['id']
        adoption_details = { 'icfrFollowup' :{
            'orderId' : order_id,
            'commerceItemId' : commerce_item_id,
            'adoptionDetails' : {
                'status' : 'adopted',
                'courseAdoptionStatus' : 'Essential',
                'adoptedAs' : 'Essential',
                'reason' : 'I was drinking.',
                'courseInstanceId' : course_instance['id'],
                'courseInstance' : {
                    "enrollment": "10000",
                    "endDate": "2017-12-30",
                }
            }
        } }
        url = API_ROOT + ('/profile/%s/icfr/followup' % self.profile_id)
        response = requests.post(url, data=json.dumps(adoption_details), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.text)
        new_course_instance = response_data['icfrFollowup']['adoptionDetails']['courseInstance']
        self.assertEquals('10000', new_course_instance['enrollment'])
        # make sure it's still the same course
        self.assertEquals(start_ci_id, new_course_instance['id'])
        response_data = json.loads(response.text)
        new_course_instance = response_data['icfrFollowup']['adoptionDetails']['courseInstance']
        self.assertEquals('10000', new_course_instance['enrollment'])
        # make sure it's still the same course
        self.assertEquals(start_ci_id, new_course_instance['id'])

    def test_add_followup_not_adopted(self):
        order = self.create_order()
        order_id = order['icfrorder']['id']
        commerce_item = order['icfrorder']['commerceItems']
        commerce_item_id = order['icfrorder']['commerceItems'].keys()[0]
        course_instance = order['icfrorder']['commerceItems'][commerce_item_id]['courseInstances'][0]
        adoption_details = { 'icfrFollowup' :{
            'orderId' : order_id,
            'commerceItemId' : commerce_item_id,
            'adoptionDetails' : {
                'status' : 'not_adopted',
                'courseAdoptionStatus' : 'Not Adopted',
                'courseInstanceId' : course_instance['id']
            }
        } }
        url = API_ROOT + ('/profile/%s/icfr/followup' % self.profile_id)
        response = requests.post(url, data=json.dumps(adoption_details), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)

    def test_add_followup_new_course(self):
        order = self.create_order()
        order_id = order['icfrorder']['id']
        commerce_item = order['icfrorder']['commerceItems']
        commerce_item_id = order['icfrorder']['commerceItems'].keys()[0]
        course_instance = order['icfrorder']['commerceItems'][commerce_item_id]['courseInstances'][0]

        new_course = self.create_course(data={'name' : 'Statistical Methods Seminar'})
        logging.debug('old course id: ' + course_instance['courseId'])
        logging.debug('new course id: ' + new_course['id'])
        adoption_details = { 'icfrFollowup' :{
            'orderId' : order_id,
            'commerceItemId' : commerce_item_id,
            'adoptionDetails' : {
                'status' : 're_new',
                'adoptedAs' : 'Essential',
                'courseAdoptionStatus' : 'Essential',
                'courseInstanceId' : course_instance['id'],
                'courseId' : new_course['id'],
                'courseInstance' : {
                    'courseId' : new_course['id'],
                    'departmentId' : course_instance['departmentId'],
                    'startDate': '2015-09-01',
                    'endDate' : '2016-03-02',
                    'decisionDate' : '',
                    'readingType' : 'Essential Reading',
                    'readopted' : 're_new',
                    'enrollment' : '300'
                }
            }
        } }
        url = API_ROOT + ('/profile/%s/icfr/followup' % self.profile_id)
        response = requests.post(url, data=json.dumps(adoption_details), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        self.assertEquals(data['icfrFollowup']['adoptionDetails']['courseInstance']['readopted'], 're_new')
        self.assertEquals(data['icfrFollowup']['adoptionDetails']['courseInstance']['courseId'], new_course['id'])


    def test_add_followup_new_course_DWEB910(self):
        order = self.create_order()
        order_id = order['icfrorder']['id']
        commerce_item = order['icfrorder']['commerceItems']
        commerce_item_id = order['icfrorder']['commerceItems'].keys()[0]
        course_instance = order['icfrorder']['commerceItems'][commerce_item_id]['courseInstances'][0]

        new_course = self.create_course(data={'name' : 'Statistical Methods Seminar'})
        adoption_details = { "icfrFollowup" : {
            "orderId" : order_id,
            "commerceItemId" : commerce_item_id,
            "adoptionDetails":{
             "status" : "re_new",
             "adoptedAs" : "Supplementary",
             "courseAdoptionStatus" : "Supplemental",
             "enrollment" : "70",
             "courseInstanceId" : course_instance['id'],
             "reason" : "Clinical palcadfasdffasdfasdf",
             "courseId" : new_course['id'],
             "otherTexts" : "",
             "startDate" : "2015-07-01",
             "endDate" : "2016-01-01",
             "courseInstance":{
                "courseId" : new_course['id'],
                "enrollment" : "70",
                "startDate" : "2015-07-01",
                "endDate" : "2016-01-01"
             }
            }
        } }
        url = API_ROOT + ('/profile/%s/icfr/followup' % self.profile_id)
        response = requests.post(url, data=json.dumps(adoption_details), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        self.assertEquals(data['icfrFollowup']['adoptionDetails']['courseInstance']['readopted'], 're_new')
        self.assertEquals(data['icfrFollowup']['adoptionDetails']['courseInstance']['courseId'], new_course['id'])


class TaxTests(AbstractTestCase):
    def test_us_has_ship_tax(self):
        order = { 'order' : {
            'locale':'en_US',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Miami',
                'postalCode': '33131',
                'state': 'FL',
                'country':'US'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Miami',
                'postalCode': '33131',
                'state': 'FL',
                'country':'US'
            },
            'amount' :{
                'amount' : 50.0
            },
            "commerceItems": [ {
                "productId": "Book242528",
                "catalogRefId": "Book Product72468",
                "attributes" : {
                    "bindingType" : 'P'
                },
                "quantity": 1,
                "ISBN-13": "9781483345642",
                "catalogKey": "en_US",
                'amount' : {
                    'listPrice' : 25.0,
                    'amount' : 25.0
                }
            } ],
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        tax_lines = order['shipping']['amount']['taxLines']
        self.assertEquals(2, len(tax_lines))
        total_tax = reduce(lambda x, y: x + y, [ x['rate'] for x in tax_lines ] )
        self.assertEquals(7.0, total_tax)

    def test_us_no_ship_tax(self):
        order = { 'order' : {
            'locale':'en_US',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Tuscaloosa',
                'postalCode': '35401',
                'state': 'AL',
                'country':'US'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Tuscaloosa',
                'postalCode': '35401',
                'state': 'AL',
                'country':'US'
            },
            'amount' :{
                'amount' : 50.0
            },
            "commerceItems": [ {
                "productId": "Book242528",
                "catalogRefId": "Book Product72468",
                "attributes" : {
                    "bindingType" : 'P'
                },
                "quantity": 1,
                "ISBN-13": "9781483345642",
                "catalogKey": "en_US",
                'amount' : {
                    'listPrice' : 25.0,
                    'amount' : 25.0
                }
            } ],
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        self.assertTrue(order['shipping']['amount'].get('taxLines') is None)


    def test_us_journal_tax(self):
        """journal taxes are a function of subscriptionType and frequency"""
        order = { 'order' : {
            'locale':'en_US',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Beverly Hills',
                'postalCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Beverly Hills',
                'postalCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            "commerceItems": [{
                "productId": "Journal201678",
                "catalogRefId": "6Journal201678P11",
                "quantity": 1,
                "attributes" : {
                    "frequency": 3,
                    "subscriptionType": 6,
                },
                "catalogKey": "en_US",
                "amount": {
                    "listPrice": 100.0,
                    "discounts": [],
                    "amount": 100.0
                }
            }
            ],
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 0.0,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        tax_lines = order.get('commerceItems')[0].get('amount').get('taxLines')
        self.assertTrue(len(tax_lines) == 3)
        total_tax = reduce(lambda x, y: x + y, [ x['rate'] for x in tax_lines ] )
        self.assertEquals(9.0, total_tax)

    def test_canada_journal_tax(self):
        """canadian journal are taxed"""
        order = { 'order' : {
            'locale':'en_US',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '3738 Selinger Cr',
                'city': 'Regina',
                'postalCode': 'S4G4A7',
                'state': 'SK',
                'country':'CA'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '3738 Selinger Cr',
                'city': 'Regina',
                'postalCode': 'S4G4A7',
                'state': 'SK',
                'country':'CA'
            },
            "commerceItems": [{
                "productId": "Journal201678",
                "catalogRefId": "6Journal201678P11",
                "quantity": 1,
                "attributes" : {
                    "frequency": 3,
                    "subscriptionType" : 6
                },
                "catalogKey": "en_US",
                "amount": {
                    "listPrice": 100.0,
                    "discounts": [],
                    "amount": 100.0
                }
            }
            ],
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 0.0,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        tax_lines = order.get('commerceItems')[0].get('amount').get('taxLines')
        self.assertTrue(len(tax_lines) == 1)
        total_tax = reduce(lambda x, y: x + y, [ x['rate'] for x in tax_lines ] )
        self.assertEquals(5.0, total_tax)


    def test_us_discount_book_tax(self):
        order = { 'order' : {
            'locale':'en_US',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Beverly Hills',
                'postalCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '1435 Brickell Ave',
                'city': 'Beverly Hills',
                'postalCode': '90210',
                'state': 'CA',
                'country':'US'
            },
            "commerceItems": [{
                "productId": "Book242528",
                "catalogRefId": "Book Product72468",
                "quantity": 1,
                "bindingType": "P",
                "ISBN-13": "9781483345642",
                "catalogKey": "en_US",
                "amount": {
                    "listPrice": 25.0,
                    "discounts": [{
                        "type" : "percent",
                        "code" : "N14AL0",
                        "amount" : 7.5
                    }],
                    "amount": 25.0
            } } ],
            "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 0.0,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        discounted_price = 17.5
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        tax_lines = order.get('commerceItems')[0].get('amount').get('taxLines')
        self.assertTrue(len(tax_lines) == 3)
        total_rate = reduce(lambda x, y: x + y, [ x['rate'] for x in tax_lines ] )
        total_tax = reduce(lambda x, y: x + y, [ x['amount'] for x in tax_lines ] )
        self.assertEquals(9.0, total_rate)
        self.assertEquals(discounted_price * total_rate / 100, total_tax)

    def test_eu_dvd_tax(self):
        price = 20.99
        order = { 'order' : {
            'locale':'en_GB',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Paris',
                'postalCode': '75667',
                'country':'FR'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Paris',
                'postalCode': '75667',
                'country':'FR'
            },
            'amount' :{
                'amount' : 50.0
            },
            "commerceItems": [ {
                "productId": "Book232669",
                "catalogRefId": "Book Product35625",
                "quantity": 1,
                "ISBN-13": "9781890455026",
                "catalogKey": "en_GB",
                'amount' : {
                    'listPrice' : price,
                    'amount' : price
                },
                'attributes' : {
                    'bindingType' : 'D'
                }
            } ],
           "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        tax_line = order['commerceItems'][0]['amount']['taxLines'][0]
        self.assertEquals(20.0, tax_line['rate'])
        self.assertEquals((20.0 / 100) * price, tax_line['amount'])

    def test_eu_journal_tax(self):
        price = 445.0
        order = { 'order' : {
            'locale':'en_GB',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Paris',
                'postalCode': '75667',
                'country':'FR'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Paris',
                'postalCode': '75667',
                'country':'FR'
            },
            "commerceItems": [ {
                "productId": "Journal201764",
                "catalogRefId": "14Journal201764P11",
                "quantity": 1,
                "catalogKey": "en_GB",
                'amount' : {
                    'listPrice' : price,
                    'amount' : price
                },
                'attributes' : {
                    'subscriptionType' : 14,
                    'frequency' : 4
                }
            } ],
           "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        logging.debug(response.text)
        tax_line = order['commerceItems'][0]['amount']['taxLines'][0]
        self.assertEquals(20.0, tax_line['rate'])
        self.assertEquals((20.0 / 100) * price, tax_line['amount'])

    def test_eu_journal_electronic_tax(self):
        price = 445.0
        order = { 'order' : {
            'locale':'en_GB',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Split',
                'postalCode': '75667',
                'country':'HR'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Split',
                'postalCode': '75667',
                'country':'HR'
            },
            "commerceItems": [ {
                "productId": "Journal201764",
                "catalogRefId": "14Journal201764P11",
                "quantity": 1,
                "catalogKey": "en_GB",
                'amount' : {
                    'listPrice' : price,
                    'amount' : price
                },
                'attributes' : {
                    'subscriptionType' : 22,
                    'frequency' : 4
                }
            } ],
           "shipping" : {
              'code' : '2293847',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        logging.debug(response.text)
        tax_line = order['commerceItems'][0]['amount']['taxLines'][0]
        self.assertEquals(25.0, tax_line['rate'])
        self.assertEquals((25.0 / 100) * price, tax_line['amount'])

    def test_eu_insure_tax_is_based_on_shipaddress(self):
        price = 445.0
        order = { 'order' : {
            'locale':'en_GB',
            'profileId' : 23094823948,
            'shippingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Paris',
                'postalCode': '75667',
                'country':'DE'
            },
            'billingAddress' : {
                'firstName': 'Test',
                'lastName': 'Test',
                'telephone': '0123456789',
                'address1': '10 rue de la marne',
                'city': 'Paris',
                'postalCode': '75667',
                'country':'GB'
            },
            "commerceItems": [ {
                "productId": "Journal201764",
                "catalogRefId": "14Journal201764P11",
                "quantity": 1,
                "catalogKey": "en_GB",
                'amount' : {
                    'listPrice' : price,
                    'amount' : price
                },
                'attributes' : {
                    'subscriptionType' : 14,
                    'frequency' : 4
                }
            } ],
           "shipping" : {
              'code' : '2293847203948',
              'title' : 'USPS Ground',
              'amount' : {
                'amount' : 6.95,
              }
            }
          } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        logging.debug(response.text)
        tax_line = order['commerceItems'][0]['amount']['taxLines'][0]
        self.assertEquals(19.0, tax_line['rate'])

    def test_tax_rounding(self):
        user = util.random_user()['profile']
        order = { "order" : {
            "shippingAddress" : {
                "firstName" : "Brian",
                "lastName" : "Yeitz",
                "address1" : "4000 Central Florida Blvd",
                "address2" : "PO Box 161999",
                "city" : "Orlando",
                "state" : "FL",
                "postalCode" : "32816",
                "country" : "US",
                "billingAddress" : False,
                "shippingAddress" : False,
                "pobox" : False,
                "homeAddress" : False,
                "residential" : False
            },
            "billingAddress" : {
                "firstName" : "Brian",
                "lastName" : "Yeitz",
                "address1" : "4000 Central Florida Blvd",
                "address2" : "PO Box 161999",
                "city" : "Orlando",
                "state" : "FL",
                "postalCode" : "32816",
                "country" : "US",
                "billingAddress" : True,
                "shippingAddress" : True,
                "pobox" : False,
                "homeAddress" : False,
                "residential" : False
            },
            "shipping" : {
                "amount" : {
                    "listPrice" : 9.0,
                    "amount" : 9.0,
                    "salePrice" : 0.0,
                    "onSale" : False,
                    "verify" : True
                },
                "code" : "USPS- PRIORITY MAIL",
                "title" : "USPS Priority Mail"
            },
            "commerceItems" : [ {
                "productId" : "Book10705",
                "catalogRefId" : "Book Product8282",
                "quantity" : 1,
                "catalogKey" : "en_US",
                "amount" : {
                    "listPrice" : 100.0,
                    "amount" : 100.0,
                    "salePrice" : 0.0,
                    "onSale" : False,
                    "verify" : True
                },
                "attributes" : {
                    "bindingType" : "P"
                }
            } ],
            "locale" : "en_US",
            "amount" : {
                "amount" : 100.0,
                "verify" : True
            },
            'email' : user['email']
        } }
        url = API_ROOT + ('/tax/calculate')
        response = requests.post(url, data=json.dumps(order), **rconf)
        order = json.loads(response.text)['order']
        logging.debug(response.text)

class TaxonomyTests(AbstractTestCase):

    def verify(self, url):
        url = ('%s/taxonomy/%s?test=true' % (API_ROOT , url))
        response = requests.get(url, **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.text)
        key = data.keys()[0]
        self.assertTrue(len(data.get(key)) > 0)

    def test_course_categories(self):
        result = self.verify('courses')

    def test_disciplines(self):
        result = self.verify('disciplines')

    def test_series(self):
        result = self.verify('series')

    def test_department_types(self):
        result = self.verify('departmenttypes')

    def test_bundle_types(self):
        result = self.verify('bundletypes')

    def test_binding_types(self):
        result = self.verify('bindingtypes')

    def test_sales_reps(self):
        result = self.verify('salesreps')

    def test_audience(self):
        result = self.verify('audience')

    def test_promocodes(self):
        result = self.verify('promotioncodes')

if __name__ == '__main__':
    logging.debug('running tests against %s' % API_ROOT)
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
