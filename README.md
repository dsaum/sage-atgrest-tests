ATG rest Web Service Test Suite
=========

Validation/integration tests for the ATG rest services.

*Note*: To run the MS CRM -> Drupal tests, just make sure you've covered the Requirements & Installation steps and then skip to the section *CRM Tests*. 


Requirements
-----------
A python install (>=2.6)
python virtualenv (recommended)

Virtualenv is included in most python installs. If it's not present, install it like this:

    pip install virtualenv


Installation
-----------

Run the following commands to create a virtualenv and then to retrieve the required python dependencies.

    virtualenv .
    bin/python setup.py install
    
Configuration
-------------

Create a configuration file called env_config.py by copying sample.env_config.py.

    cp sample.env_config.py env_config.py

The only variables you need to change are API_ROOT and AUTH.

Set AUTH to the credentials for the server you are interacing with:

    AUTH = ('user', 'p@55w0rd')
    
Set API_ROOT to point to the API root on the machine you're looking to run your tests against.

Typically, against staging or QA that will look like:

    API_ROOT = 'https://icat.stg01.sagepub.com/api'

In development, you might bypass Apache to hit the service directly like so:

    API_ROOT = 'http://ltdvsatg14.us.sagepub.org:8080/atgrest/rest'
    
By default, the log level is set to WARN. As the script runs, you won't see any messages (as long as everything works). To see the requests and responses being made and received, set the log level to DEBUG.

    logging.basicConfig(level=logging.DEBUG)

Running the tests
-----------------

Running the tests is as simple as executing:

    bin/python run.py

The following examples show how to run one suite of tests or even just a single test:

    # To run the profile test suite
    bin/python -m unittest run.ProfileTests
    
    # Or the course suite
    bin/python -m unittest run.CourseTests
    
    # Say you just want to test the creation of new addresses
    bin/python -m unittest run.AddressTests.test_create_address
    

CRM Tests
---------

To run the CRM test, run:

    bin/python drupal_crm.py
