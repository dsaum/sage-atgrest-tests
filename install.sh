#! /bin/bash
if ! type "pip" > /dev/null; then
   pip install virtualenv
fi
if [ ! -f bin/python ]; then
   virtualenv .
fi
bin/python setup.py install
