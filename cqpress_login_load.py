import random
import json
import logging

from locust.test.testcases import LocustTestCase
from locust import HttpLocust, TaskSet, task

HEADERS = { 'Content-Type': 'application/json' }

Accounts = [{'credentials':{'email':'hesdem@gmail.com','password':'password'}, 'valid': False},\
             {'credentials':{'email':'bgilliland@sagepub.com','password':'123456'}, 'valid': False}]


class ProfileTasks(TaskSet):

    @task
    def test_login(self):
        url = '/User/Login'
        user = random.choice(Accounts)
        data = user.get('credentials')
        valid = user.get('valid')
        with self.client.post(url, json.dumps(data),
                catch_response=True,
                name='/User/Login',
                headers = { 'Content-Type': 'application/json' }) as res:
            print res.text
            if res.status_code == 200:
                res.success()


class CQPressUser(HttpLocust):
    task_set = ProfileTasks
    min_wait = 200
    max_wait = 500
    host = 'https://rest.cqpress.com/'
