import unittest
import xmlrunner
import string
import random
import requests
import sys
import env_config
import json
import logging
import util

API_ROOT = env_config.API_ROOT
HEADERS = { 'Content-Type': 'application/json' }
AUTH = requests.auth.HTTPBasicAuth(env_config.AUTH[0], env_config.AUTH[1])

rconf = { 'headers': HEADERS, 'auth': AUTH, 'verify': False }

class AbstractTestCase(unittest.TestCase):
    '''Base class for verification tests'''

    def compare(self, o1, o2, props):
        for k in props:
            if o1.has_key(k):
                self.assertEqual(o1[k], o2[k])

    def create_user(self, data):
        logging.debug('creating user: ' + str(data['profile']))
        response = requests.post(API_ROOT + '/profile', data = json.dumps(data), **rconf)
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.text)
        logging.debug('user created: %s' % response_data['profile']['id'])
        logging.debug(response.text)
        return response_data

    def create_department(self):
        department = {
            'department': {
                'address': {
                    'address1': 'Cyncoed Road',
                    'billingAddress': True,
                    'city': 'Cardiff',
                    'country': 'GB',
                    'county': 'South Glamorgan',
                    'firstName': 'Russell',
                    'institution': 'University of Wales Institute, Cardiff',
                    'lastName': 'Deacon',
                    'phoneNumber': '01222551111',
                    'postalCode': 'CF23 6XD',
                    'salutation': 'Dr',
                    'shippingAddress': True,
                    'departmentType': '10123'
                },
                'departmentTypeId' : '0369748218000000',
                'institutionId': '149000',
            }
        }
        url = API_ROOT + ('/profile/%s/departments' % self.profile_id)
        response = requests.post(url, data = json.dumps(department), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['department']

    def create_course(self, data = {}):
        self.department = self.create_department()
        course = {
            'course' : {
                'name': 'Introduction to Statistics',
                'departmentId': self.department['id'],
                'courseLevel': '13000001',
                'category1': '600',
                'category2': '610',
                'category3': '617',
                'expectedEnrollment' : 300,
                'courseNumber': 'Econ 101',
                'status' : 'PA14'
            }
        }
        course['course'].update(data)
        url = API_ROOT + '/profile/%s/courses' % self.profile_id
        response = requests.post(url, data = json.dumps(course), **rconf)
        logging.debug(response.text)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.text)['course']

class CSRTests(AbstractTestCase):
    """Tests for ICFR orders"""
    def setUp(self):
        new_user = util.random_user()
        self.user = self.create_user(new_user)['profile']
        self.profile_id = self.user['id']
        self.course = self.create_course()


    def test_create_order(self):
        """verifies the creation of a single order and double checks
           that all the fields are copied correctly"""
        order = {
            "icfrorder": {
                "commerceItems": [{
                  "ISBN-13": "9781446249185",
                  "catalogKey": "en_GB",
                  "catalogRefId": "Book Product49790",
                  "courseInstances": [{
                    "courseId": self.course['id'],
                    "courseStatus": "A",
                    "courseNum": "Hist101",
                    "decisionDate": "2014-11-05",
                    "departmentId": self.department['id'],
                    "endDate": "2014-02-01",
                    "enrollment": "23",
                    "startDate": "2014-01-01"
                  }],
                  "icfrType": "E",
                  "productId": "Book238032",
                  "quantity": 1
                }],
                "shippingAddressId": self.department['address']['id']
            }
        }

        logging.debug(json)
        url = API_ROOT + ('/profile/%s/icfr' % self.profile_id)
        response = requests.post(url, data = json.dumps(order), **rconf)

if __name__ == '__main__':
    logging.debug('running tests against %s' % API_ROOT)
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
