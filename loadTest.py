import string
import random
import sys
import env_config
import json
import logging
import util
import requests
import time

from collections import deque
from locust.test.testcases import LocustTestCase
from locust import HttpLocust, TaskSet, task

HEADERS = { 'Content-Type': 'application/json' }
AUTH = requests.auth.HTTPBasicAuth(env_config.AUTH[0], env_config.AUTH[1])
API_ROOT = env_config.API_ROOT

rconf = { 'headers': HEADERS, 'auth': AUTH, 'verify': False }

f = open('profileIds.txt', 'rt')
profile_ids = f.read().splitlines()
f.close()

f = open('profileEmails.txt', 'rt')
profile_emails = f.read().splitlines()
f.close()

queue = deque([])

class ProfileTasks(TaskSet):

    def test_get_user(self):
        url = '/profile/' + random.choice(profile_ids)
        response = self.client.get(url, name='/profile', **rconf)
        log_response(url, response)

    def test_login(self):
        data = { 'login' : {
            'email': str(random.choice(profile_emails)),
            'password': '123456'}
        }
        with self.client.post('/profile/login', json.dumps(data),
                catch_response=True,
                name='/profile/login',
                **rconf) as res:
            if res.status_code != 200:
                print res.text
            log_response('/profile/login', res)
            if res.status_code != 500:
                res.success()

    def test_create_user(self):
        user = util.random_user()
        response = self.client.post('/profile/', json.dumps(user),
                     name='/profile', **rconf)
        response_data = json.loads(response.text)['profile']
        u = {'id' : user.get('id'),
            'email' : user.get('email')}
        queue.append(u)
        log_response('/profile/', response)

    def test_update_user(self):
        if len(queue) > 0:
            u = queue.popleft()
            profile_id = u.get('id')
            email = u.get('email')
            if profile_id != None and email != None:
                response = self.client.put('/profile/' + profile_id,
                    util.random_user(data={'email' : email}),
                    name='/profile', **rconf)
                log_response('/profile/', response)

    def test_get_user_addresses(self):
        url = '/profile/%s/addresses' %  (random.choice(profile_ids))
        response = self.client.get(url, name='/profile/addresses', **rconf)
        log_response(url, response)

    def test_reset_user_password(self):
        url = '/profile/resetPassword/' + random.choice(profile_emails)
        with self.client.post(url,
                    name='/profile/resetPassword/', catch_response=True, **rconf) as res:
            if res.status_code != 200:
                print res.text
            log_response('/profile/login', res)
            if res.status_code != 500:
                res.success()

    def test_update_user_password(self):
        url = '/profile/%s/updatePassword' %  (random.choice(profile_ids))
        data = { 'passwordUpdate' : {
            'password' : '123456',
            'newPassword' : '123456' }
        }
        with self.client.post(url, json.dumps(data),
                name='/profile/updatePassword', catch_response=True, **rconf) as res:
            if res.status_code != 200:
                print res.text
            log_response('/profile/login', res)
            if res.status_code != 500:
                res.success()

    def test_add_department(self):
        url = '/profile/%s/departments' %  (random.choice(profile_ids))
        dept = util.department()
        response = self.client.post(url, name='/profile/departments',
                    data=json.dumps(dept), **rconf)
        log_response(url, response)

    def test_get_user_departments(self):
        url = '/profile/%s/departments' %  (random.choice(profile_ids))
        response = self.client.get(url, name='/profile/departments', **rconf)
        log_response(url, response)

    def test_update_department(self):
        url = '/profile/%s/departments' %  (random.choice(profile_ids))
        dept = util.department()
        response = self.client.post(url, data=json.dumps(dept),
                    name='/profile/departments', **rconf)
        created_dept = json.loads(response.text)
        # wait between 0, 3 seconds
        time.sleep(3 * random.random())
        update_url = '%s/%s' % (url, created_dept['department']['id'])
        response = self.client.put(update_url, data=json.dumps(created_dept),
                    name='/profile/departments', **rconf)
        log_response(url, response)

    def test_add_course(self):
        profile_id = random.choice(profile_ids)
        dept = util.department()
        url = '/profile/%s/departments' %  profile_id
        dept_response = self.client.post(url, data=json.dumps(dept),
                    name='/profile/departments', **rconf)
        created_dept = json.loads(dept_response.text)['department']
        url = '/profile/%s/courses' %  profile_id
        course = util.course({'departmentId': created_dept['id']})
        response = self.client.post(url, data=json.dumps(course),
                    name='/profile/courses', **rconf)
        log_response(url, response)

    def test_get_user_courses(self):
        url = '/profile/%s/courses' %  (random.choice(profile_ids))
        response = self.client.get(url, name='/profile/courses', **rconf)
        log_response(url, response)

    def test_update_course(self):
        profile_id = random.choice(profile_ids)
        dept = util.department()
        url = '/profile/%s/departments' %  profile_id
        dept_response = self.client.post(url, data=json.dumps(dept),
                    name='/profile/departments', **rconf)
        created_dept = json.loads(dept_response.text)['department']
        url = '/profile/%s/courses' %  profile_id
        course = util.course({'departmentId': created_dept['id']})
        response = self.client.post(url, data=json.dumps(course),
                    name='/profile/courses', **rconf)
        created_course = json.loads(response.text)['course']
        updated_course = util.course(created_course)
        # wait between 0, 3 seconds
        time.sleep(3 * random.random())
        update_url = '%s/%s' % (url, created_course['id'])
        response = self.client.put(update_url, data=json.dumps(updated_course),
                    name='/profile/courses/', **rconf)
        log_response(url, response)

    def create_ecomm_order_anonymous(self):
        email = util.random_user()['profile'].get('email')
        order = util.order(email)
        url = '/orders'
        response = self.client.post(url, data=json.dumps(order), name='/orders', **rconf)
        log_response(url, response)

    def test_create_icfr(self):
        # create a user (specify extended=True to make sure an address is created)
        user = util.random_user(extended=True)
        response = self.client.post('/profile/', json.dumps(user),
                     name='/profile', **rconf)
        profile = json.loads(response.text)['profile']
        profile_id = profile['id']
        time.sleep(3 * random.random())

        # add a department
        url = '/profile/%s/departments' %  profile_id
        dept_data = util.department()
        response = self.client.post(url, data=json.dumps(dept_data),
                    name='/profile/departments', **rconf)
        dept = json.loads(response.text)['department']
        time.sleep(3 * random.random())

        # add a course
        url = '/profile/%s/courses' %  profile_id
        course_data = util.course({'departmentId': dept['id']})
        response = self.client.post(url, data=json.dumps(course_data),
                    name='/profile/courses', **rconf)
        course = json.loads(response.text)['course']
        time.sleep(3 * random.random())

        # create the order
        order = util.icfr(profile, dept, course)

        url = API_ROOT + '/profile/%s/icfr' % profile_id
        response = self.client.post(url, data = json.dumps(order),
                    name="/profile/icfr", **rconf)

    tasks = {
        test_get_user: 40,
        test_login: 20,
        test_create_user: 10,
        test_update_user: 10,
        test_get_user_addresses: 10,
        test_reset_user_password: 2,
        test_update_user_password: 5,
        test_add_department: 5,
        test_update_department: 2,
        test_get_user_departments: 10,
        test_add_course: 5,
        test_get_user_courses: 10,
        test_update_course: 5,
        create_ecomm_order_anonymous: 1,
        test_create_icfr: 2
    }


def log_response(url, r):
    msg = '%s\n-----------------\nstatus: %d\ncontent:\n%s ' %\
            ( url, r.status_code, r.content )
    logging.debug(msg)

class TaxonomyTasks(TaskSet):

    @task(1)
    def promotioncodes(self):
        response = self.client.get('/taxonomy/promotioncodes?test=true', **rconf)
        log_response(response)


class WebsiteUser(HttpLocust):
    task_set = ProfileTasks
    min_wait = 200
    max_wait = 500
    host = API_ROOT
