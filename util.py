import string
import random
import json
import env_config
import logging

# Utility methods
def random_string(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size)).lower()

def get_username(base):
    '''generates new random username of the form base[some_random_string]'''
    return base + random_string()

# this will not work in python >= 3
def merge(a, b):
    if type(a) == dict and type(b) == dict and b and len(b) > 0:
        return dict(a.items() + b.items())
    return a

def random_user(data={}, extended=True):
    cfg = env_config
    prefix = cfg.EMAIL_PREFIX or 'test+'
    domain = cfg.EMAIL_DOMAIN or '@sagepub.com'
    password = cfg.USER_PASSWORD or random_string()
    email = get_username(prefix) + domain
    logging.debug('random user: %s/%s' % (email, password))
    return _populate_profile(email, password, data, extended)

def _populate_profile(email, password, data, extended):
    profile = {
        'email' : email,
        'password' : password,
        'firstName': 'Jill',
        'middleName': 'Lynn',
        'lastName': 'Doe',
        'telephone': '0123456789',
        'title': 'Professor',
        'organisationName': 'Bob Jones University',
        'areasOfInterest' : ['A00','AA0','A70'],
        'roles' : ['77', '81'],
        'locale': 'en_US',
        'country' : 'US',
    }
    if extended:
        profile['address'] = {
            'firstName': 'Sarah',
            'lastName': 'Warnes',
            'institution': 'California Lutheran University',
            'roomNumber': '12',
            'department': 'Computer Science Dept,',
            'telephone': '0123456789',
            'address1': '10 Sachem St.',
            'address2': 'Room 10',
            'address3': 'PO Box 45',
            'city': 'Los Angeles',
            'postCode': '90210',
            'shippingAddress' : True,
            'state': 'CA',
            'country':'US'
        }
    return { 'profile' : merge(profile, data) }

def department(data={}):
    department = {
        'address': {
            'address1': 'Cyncoed Road',
            'billingAddress': True,
            'city': 'Cardiff',
            'country': 'GB',
            'county': 'South Glamorgan',
            'firstName': 'Russell',
            'institution': 'University of Wales Institute, Cardiff',
            'lastName': 'Deacon',
            'phoneNumber': '01222551111',
            'postalCode': 'CF23 6XD',
            'salutation': 'Dr',
            'shippingAddress': True,
            'departmentType': '10123'
        },
        'departmentTypeId' : '0369748218000000',
        'institutionId': '149000'
    }
    return { 'department' : merge(department, data) }


def course(data={}):
    course = {
        'name': 'Introduction to Statistics',
        'courseLevel': '13000001',
        'category1': '600',
        'category2': '610',
        'category3': '617',
        'expectedEnrollment' : 300,
        'courseNumber': 'Econ 101',
        'status' : 'PS16'
    }
    return { 'course' : merge(course, data) }

def order(email, data={}):
    order = {
        'email' : email,
        'locale':'en_US',
        'shippingAddress' : {
            'firstName': 'Sarah',
            'lastName': 'Warnes',
            'phoneNumber': '0123456789',
            'email' : email,
            'address1': '10 Sachem St.',
            'city': 'Los Angeles',
            'postalCode': '90210',
            'state': 'AL',
            'country':'US'
        },
        'billingAddress' : {
            'firstName': 'Sarah',
            'lastName': 'Warnes',
            'telephone': '0123456789',
            'address1': '10 Sachem St.',
            'city': 'Los Angeles',
            'postalCode': '90210',
            'state': 'AL',
            'country':'US',
            'phoneNumber': '0123456789',
            'email' : email,
        },
        "commerceItems": [ {
            "productId": "Book241785",
            "catalogRefId": "Book Product66713",
            "quantity": 1,
            "ISBN-13": "9781483344393",
            "catalogKey": "en_US",
            'amount' : {
                'listPrice' : 74.0,
                'taxLines' : [{
                    'type' : 'state',
                    'rate' : 6.0,
                    'amount' : 4.44
                }],
                'amount' : 74.0
            }
        },{
            "productId": "Book242507",
            "catalogRefId": "Book Product71974",
            "quantity": 1,
            "ISBN-13": "9781483358666",
            "catalogKey": "en_US",
            'amount' : {
                'listPrice' : 39.0,
                'amount' : 39.0,
                'taxLines' : [{
                    'type' : 'state',
                    'rate' : 6.0,
                    'amount' : 1.87

                }],
                'discounts' : [{
                  'code' : 'S05CAES',
                  'type' : 'percent',
                  'amount' : 7.80,
                  'rate' : 0.2
                }]
            }
        } ],
        'amount' :{
            'amount' : 118.94,
        },
        'payment' : {
            'paymentToken' : {
                'authorizationId' : 'tst590',
                'token' : '488616229-20150304171035-137F24B13BC03DB946A7CFD70605DECA.sbg-vm-hpp02',
                'source' : 'paymenttech',
                'txRef' : '54F7B8AE19B5998F9754BE2E7B1C816D50D05376;0',
                'attributes' : {
                  'cc_holder' : 'Jamie Dimon',
                  'cc_brand' : 'Visa',
                  'cc_bin' : '407704',
                  'cc_last4' : '1112',
                  'cc_expiry_month' : '06',
                  'cc_expiry_year' : '2015'
                }
            }
        },
        "shipping" : {
          'code' : 'UPSP-GROUND',
          'title' : 'USPS Ground',
          'amount' : {
            'amount' : 6.95,
            'taxLines' : [ {
              'type' : 'country',
              'rate' : 6.0,
              'amount' : 0.48
            } ]
          }
        }
    }
    return {'order' : merge(order, data)}

def icfr(profile, department, course, data={}):
    order = {
        "icfrorder": {
            "commerceItems": [{
              "ISBN-13": "9781446249185",
              "catalogKey": "en_GB",
              "catalogRefId": "Book Product49790",
              "courseInstances": [{
                "courseId": course['id'],
                "courseStatus": "A",
                "courseNum": "Hist101",
                "decisionDate": "2014-11-05",
                "departmentId": department['id'],
                "endDate": "2014-02-01",
                "enrollment": "23",
                "startDate": "2014-01-01"
              }],
              "icfrType": "E",
              "productId": "Book238032",
              "quantity": 1
            }],
            "shippingAddressId": department['address']['id']
        }
    }
    return order

def total_for_amount(entry):
    """returns the total price (amount + taxes) for an entry with an amount"""
    amount = entry['amount']
    total = amount['amount']
    if amount.get('taxLines') is not None:
        total += reduce(lambda x, y: x + y, [ x['amount'] for x in amount.get('taxLines') ] )
    return total


