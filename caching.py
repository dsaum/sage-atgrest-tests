# coding: utf-8

import unittest
import xmlrunner
import string
import random
import requests
import sys
import env_config
import json
import logging
import util
import datetime

SERVERS = [ 'http://sagesavprdapp01vm.sagepub.colo:8280/atgrest/rest',
            'http://sagesavprdapp02vm.sagepub.colo:8280/atgrest/rest']
HEADERS = { 'Content-Type': 'application/json' }
AUTH = requests.auth.HTTPBasicAuth(env_config.AUTH[0], env_config.AUTH[1])

rconf = { 'headers': HEADERS, 'auth': AUTH, 'verify': False }

class CacheTests(unittest.TestCase):
    def test_update_address_through_dept(self):
        user_data = util.random_user()
        dept_data = util.department()

        # create the user on app01
        url = SERVERS[0] + ('/profile')
        user_response = requests.post(url, data = json.dumps(user_data), **rconf)
        user = json.loads(user_response.text).get('profile')

        # create the dept on app01
        url = SERVERS[0] + ('/profile/%s/departments' % user.get('id'))
        dept_response = requests.post(url, data = json.dumps(dept_data), **rconf)
        self.assertEqual(dept_response.status_code, 200)
        dept_data = json.loads(dept_response.text)

        # warm up the cache on app02 by grabbing the user's departments
        url = SERVERS[1] + ('/profile/%s/departments' % user.get('id'))
        dept_response = requests.get(url, **rconf)
        logging.debug(dept_response.text)

        # modify the department and address
        dept_id = dept_data['department']['id']
        addr_id = dept_data['department']['address']['id']
        dept_data['department']['name'] = 'Mathematics & Statistics'
        dept_data['department']['address']['building'] = 'One Earls Court'
        dept_data['department']['address']['roomNumber'] = '25'
        dept_data['department']['address']['address1'] = '8 Cottage St'
        dept_data['department']['address']['city'] = 'Brighton'
        dept_data['department']['address']['postalCode'] = '06511'

        # issue the update to app01
        url = SERVERS[0] + ('/profile/%s/departments/%s' %
                     (user.get('id'), dept_id))
        dept_response = requests.put(url, data = json.dumps(dept_data), **rconf)

        # check the response on app01
        url = SERVERS[0] + ('/profile/%s/departments' % user.get('id'))
        dept_response = requests.get(url, **rconf)
        logging.debug('------------------  APP01 ----------------------')
        logging.debug(dept_response.text)

        # check the response on app02
        url = SERVERS[1] + ('/profile/%s/departments' % user.get('id'))
        dept_response = requests.get(url, **rconf)
        logging.debug('------------------  APP02 ----------------------')
        logging.debug(dept_response.text)

        # check the addresses on app02
        url = SERVERS[1] + ('/profile/%s/addresses' % user.get('id'))
        addr_response = requests.get(url, **rconf)
        logging.debug('------------------  APP02 ----------------------')
        logging.debug(addr_response.text)

if __name__ == '__main__':
    logging.debug('running tests against '  + str(SERVERS))
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
