# coding: utf-8

import unittest
import xmlrunner
import string
import random
import requests
import sys
import env_config
import json
import logging
import util
import datetime

API_ROOT = 'http://crmdrupalwebapisvc.azurewebsites.net/api'
HEADERS = { 'Content-Type': 'application/json' }

rconf = { 'headers': HEADERS, 'verify': False }

class AbstractTestCase(unittest.TestCase):
    '''Base class for verification tests'''

    def compare(self, o1, o2, props):
        errors = []
        for k in props:
            if o1.has_key(k):
                if not o2.has_key(k):
                    errors.append('response missing ' + k)
                elif o1.get(k) != o2.get(k):
                    errors.append('response value for [%s] differs (%s vs %s)' % (k, o1.get(k), o2.get(k)))
        if len(errors) > 0:
            return False, 'Errors: \n%s' % ('\n'.join(errors))
        return True, []

class ProfileTests(AbstractTestCase):

    def test_get_user(self):
        expected_vals = { 'Profile': {
            'id' : '10002',
            'email': 'susanking@unc.edu',
            'salutation': None,
            'firstName': 'Susan',
            'middleName': None,
            'lastName': 'King',
            'organisationName': 'Yale University',
            'title': 'Professor of History',
            'areasOfInterest': ['TBD', 'TBD'],
            'roles': ['TBD', 'TBD'],
            'telephone': '805-555-1212',
            'country': 'GB',
            'locale': 'en_GB',
            'registrationDate': None,
            'lastActivity': None,
            'emailPromotions': 'false',
            'mailPromotions': 'false',
            'phonePromotions': 'false',
            'acceptIcfrPolicy': 'yes',
            'allowICRFRequests': 'allow',
            'userType' : 'sageUser',
            'billToNewAddress': False,
            'registrationDate': '2000-03-22T08:00:00.000+0000',
            'lastActivity': '2012-05-16T07:25:22.000+0000',
        }}
        response = requests.get(API_ROOT + '/profile/10002')
        data = json.loads(response.text)
        compare_keys = ['id', 'country','locale','acceptIcfrPolicy',
                    'allowICRFRequests','areasOfInterest',
                    'billToNewAddress','email','emailPromotions',
                    'firstName','id','lastActivity','lastName',
                    'mailPromotions','middleName','phonePromotions',
                    'registrationDate','roles','salutation',
                    'telephone','userType', 'registrationDate', 'lastActivity',
                    'title', 'organisationName']
        result, err = self.compare(expected_vals.get('Profile'), data.get('Profile')[0], compare_keys)
        if not result:
            logging.error(err)
            self.fail('differences found')

if __name__ == '__main__':
    logging.debug('running tests against %s' % API_ROOT)
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
